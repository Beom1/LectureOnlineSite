package service;

import java.util.List;

import domain.Instructor;

public interface InstructorService {
	
	boolean register(Instructor instructor);
	Instructor find(String instructorNickName);
	List<Instructor> findAll();
//	boolean modify(String instructorNickName, String instructorName, String instructorProfile);
	boolean modify(Instructor instructor);
	boolean remove(String instructorNickName);
//	List<Instructor> findByNick(String nickName);
}
