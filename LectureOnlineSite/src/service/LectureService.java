package service;

import java.util.List;

import domain.Evaluation;
import domain.Lecture;

public interface LectureService{
	
	boolean register(Lecture lecture);
	Lecture find(int lectureNum);
	List<Lecture> findAll();
	boolean modify(Lecture lecture);
	boolean remove(int lectureNum);
	boolean Evaluate(Evaluation evaluation);
	Evaluation findEvaluation(int evaluateNum);
	List<Evaluation> findAllEvaluation();
	List<Evaluation> findList(int lectureNum); ///추가된부분
	int findLastIndex();
	int findEvaluationLastIndex();
	boolean basketAdd(int lectureNum, String userId);
	
}
