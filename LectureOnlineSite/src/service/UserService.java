package service;

import java.util.List;

import domain.Lecture;
import domain.Order;
import domain.User;

public interface UserService {
	String login(String userId); //로그인
	boolean register(User user); //회원가입
	User find(String userId); //한명 찾기
	List<User> findAll(); //모두찾기
	boolean modify(User user); //정보 수정
	boolean leave(String userId); //회원탈퇴
	List<Order> findMyOrder(String userId); //주문리스트
	List<Lecture> findBasketLecture(String userId); //강의리스트
	List<Order> findAllOrder();
	boolean orderRegister(Order order);
}
