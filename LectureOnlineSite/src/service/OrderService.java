package service;

import java.util.List;

import domain.Order;

public interface OrderService {
	boolean pay();
	List<Order> complete(int orderNum);
	List<Order> salesCheck(int orderNum);
}
