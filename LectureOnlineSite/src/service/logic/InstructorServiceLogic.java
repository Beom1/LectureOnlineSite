package service.logic;

import java.util.List;

import domain.Instructor;
import service.InstructorService;
import store.InstructorStore;
import store.logic.InstructorStoreLogic;

public class InstructorServiceLogic implements InstructorService {
		
	private InstructorStore instructorStore;
	
	public InstructorServiceLogic() {
		instructorStore = new InstructorStoreLogic();
	}
	
	@Override
	public boolean register(Instructor instructor) {
		return instructorStore.create(instructor);
	}

	@Override
	public Instructor find(String instructorNickName) {
		
		Instructor instructor = instructorStore.retrieve(instructorNickName);
		return instructor;
			
	}

	@Override
	public List<Instructor> findAll() {
		return instructorStore.retrieveAll();
	}

	@Override
	public boolean modify(Instructor instructor) {
		return instructorStore.update(instructor);
	}

	@Override
	public boolean remove(String instructorNickName) {
		return instructorStore.delete(instructorNickName);
	}

//	@Override
//	public List<Instructor> findByNick(String nickName) {
//		
//		return instructorStore.readByNick(nickName);
//	}

}
