package service.logic;

import java.util.List;

import domain.Order;
import service.OrderService;
import store.LectureStore;
import store.OrderStore;
import store.logic.LectureStoreLogic;
import store.logic.OrderStoreLogic;



public class OrderServiceLogic implements OrderService{
	
	private OrderStore orderStore;
	private LectureStore lectureStore;
	
	public OrderServiceLogic() {
		orderStore=new OrderStoreLogic();
		lectureStore=new LectureStoreLogic();
	}

	@Override
	public boolean pay() {
		return true;
	}

	@Override
	public List<Order> complete(int orderNum) {
		return orderStore.retrieveAll();
	}

	@Override
	public List<Order> salesCheck(int orderNum) {	
		return orderStore.retrieveAll();
	}


}
