package service.logic;

import java.util.List;

import domain.Evaluation;
import domain.Lecture;
import service.LectureService;
import store.EvaluationStore;
import store.LectureStore;
import store.logic.EvaluationStoreLogic;
import store.logic.LectureStoreLogic;

public class LectureServiceLogic implements LectureService{
	
	private LectureStore lectureStore;
	private EvaluationStore evaluationStore;
	
	public LectureServiceLogic() {
		//
		this.lectureStore = new LectureStoreLogic();
		this.evaluationStore= new EvaluationStoreLogic();
	}


	@Override
	public boolean register(Lecture lecture) {
		
		return lectureStore.create(lecture);
		// TODO Auto-generated method stub
		
	}

	@Override
	public Lecture find(int lectureNum) {
		// TODO Auto-generated method stub
		return lectureStore.retrieve(lectureNum);
	}

	@Override
	public List<Lecture> findAll() {
		// TODO Auto-generated method stub
		return lectureStore.retrieveAll();
	}

	@Override
	public boolean modify(Lecture lecture) {
		return lectureStore.update(lecture);
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean remove(int lectureNum) {
		return lectureStore.delete(lectureNum);
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public int findLastIndex() {
		// TODO Auto-generated method stub
		List<Lecture> lectures = lectureStore.retrieveAll();
		int max=0;
		for(int i =0; i< lectures.size(); i++) {
			if(max<lectures.get(i).getLectureNum()) {
				max = lectures.get(i).getLectureNum();
			}
		}
		return max;
	}

	@Override
	public boolean Evaluate(Evaluation evaluation) {
		return evaluationStore.create(evaluation);
		// TODO Auto-generated method stub
		
	}

	@Override
	public Evaluation findEvaluation(int evaluationNum) {
		// TODO Auto-generated method stub
		return evaluationStore.retrieve(evaluationNum);
	}

	@Override
	public List<Evaluation> findAllEvaluation() {
		// TODO Auto-generated method stub
		return evaluationStore.retrieveAll();
	}


	@Override
	public List<Evaluation> findList(int lectureNum) {
		// TODO Auto-generated method stub
		return evaluationStore.retrieveList(lectureNum);
	}


	@Override
	public int findEvaluationLastIndex() {
		// TODO Auto-generated method stub
		List<Evaluation> evaluations = evaluationStore.retrieveAll();
		int max=0;
		for(int i =0; i< evaluations.size(); i++) {
			if(max<evaluations.get(i).getEvaluationNum()) {
				max = evaluations.get(i).getEvaluationNum();
			}
		}
		
		return max;
		
	}
	@Override
	public boolean basketAdd(int lectureNum, String userId) {
		// TODO Auto-generated method stub
		return lectureStore.basketAdd(lectureNum, userId);
	}




}
