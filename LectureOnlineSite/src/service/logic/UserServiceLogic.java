package service.logic;

import java.util.List;

import domain.Lecture;
import domain.Order;
import domain.User;
import service.UserService;
import store.LectureStore;
import store.OrderStore;
import store.UserStore;
import store.logic.LectureStoreLogic;
import store.logic.OrderStoreLogic;
import store.logic.UserStoreLogic;

public class UserServiceLogic implements UserService {
	
	private UserStore userStore;
	private OrderStore orderStore;
	private LectureStore lectureStore;
	
	public UserServiceLogic() {
		userStore = new UserStoreLogic();
		orderStore = new OrderStoreLogic();
		lectureStore = new LectureStoreLogic();
	}
	
	@Override
	public boolean register(User user) {
		return userStore.create(user);	
	}

	@Override
	public User find(String userId) {
		return userStore.retrieve(userId);
	}

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return userStore.retrieveAll();
	}

	@Override
	public boolean modify(User user) {
		return userStore.update(user);
	}

	@Override
	public boolean leave(String userId) {
		return userStore.delete(userId);
		
	}

	@Override
	public List<Order> findMyOrder(String userId) {
		// TODO Auto-generated method stub
		return orderStore.retrieveAll();
	}

	@Override
	public List<Lecture> findBasketLecture(String userId) {
		// TODO Auto-generated method stub
		return lectureStore.myBasket(userId);
	}

	@Override
	public String login(String userId) {
		// TODO Auto-generated method stub
		return userStore.pwReturn(userId);
	}

	@Override
	public boolean orderRegister(Order order) {
		// TODO Auto-generated method stub
		return orderStore.create(order);
	}

	@Override
	public List<Order> findAllOrder() {
		// TODO Auto-generated method stub
		return orderStore.retrieveAll();
	}

}
