package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Instructor;
import service.InstructorService;
import service.logic.InstructorServiceLogic;

@WebServlet("/instructorSearch.do")
public class InstructorSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		boolean flag = Boolean.parseBoolean(request.getParameter("flag"));
		String searchNick = request.getParameter("searchInput");
		String userId = request.getParameter("userId");
		
			InstructorService service = new InstructorServiceLogic();

			Instructor instructor = service.find(searchNick);
			List<Instructor> instructors = new ArrayList<>();
			instructors.add(instructor);
			request.setAttribute("instructors", instructors);
			request.setAttribute("userId", userId);
			if(flag) {
				request.getRequestDispatcher("/views/instructorManager.jsp").forward(request, response);
			}
			else {
				request.getRequestDispatcher("/views/instructors.jsp").forward(request, response);
			}
		
		
	}

}
