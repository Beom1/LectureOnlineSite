package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lectureOnline.domain.User;
import lectureOnline.service.UserService;
import lectureOnline.service.UserServiceLogic;

@WebServlet("/user/modify.do")
public class UserModifyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String userId = request.getParameter("userId");

		UserService service = new UserServiceLogic();
		User newUser = new User();

		newUser = service.find(userId);
		request.setAttribute("user", newUser);
		request.getRequestDispatcher("/views/userModify.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String userId = request.getParameter("userId");
		String userName = request.getParameter("userName");
		String userPassword = request.getParameter("userPassword");
		String userEmail = request.getParameter("userEmail");
		String userBirth = request.getParameter("userBirth");
		String userPhoneNum = request.getParameter("userPhoneNum");

		UserService service = new UserServiceLogic();
		User newUser = new User(userName, userPassword, userEmail, userBirth, userPhoneNum);
		newUser.setUserId(userId);
		
		service.modify(newUser);
		response.sendRedirect(request.getContextPath());
	}

}
