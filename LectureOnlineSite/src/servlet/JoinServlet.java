package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.User;
import service.UserService;
import service.logic.UserServiceLogic;

@WebServlet("/join.do")
public class JoinServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserService service = new UserServiceLogic();
		User user = new User();
		request.setCharacterEncoding("UTF-8");
		
		user.setUserId(request.getParameter("id"));
		user.setUserPassword(request.getParameter("password"));
		user.setUserName(request.getParameter("name"));
		user.setUserBirth(request.getParameter("birth"));
		user.setUserEmail(request.getParameter("mail"));
		user.setUserPhoneNum(request.getParameter("phone"));
		
		service.register(user);
		
		response.sendRedirect(request.getContextPath()+"/views/login.jsp");
	}

}
