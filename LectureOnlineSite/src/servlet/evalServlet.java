package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/eval.do")
public class evalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String userId = request.getParameter("userId");	
		String lectureNum = request.getParameter("lectureNum");
		System.out.println("eval"+userId+lectureNum);
		if(lectureNum == null || lectureNum.trim().equals("")){
			lectureNum = "0";
			}

		
		request.setAttribute("userId", userId);
		request.setAttribute("lectureNum", lectureNum);
		request.getRequestDispatcher("/views/lectureEvaluation.jsp").forward(request, response);	
	}


}
