package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Lecture;
import service.LectureService;
import service.logic.LectureServiceLogic;

@WebServlet("/lectureManage.do")
public class LectureManageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String userId = request.getParameter("userId");
		request.setAttribute("userId", userId);
		LectureService service = new LectureServiceLogic();
		List<Lecture> lectures = service.findAll();
		request.setAttribute("lectures", lectures);
		request.getRequestDispatcher("/views/lectureManage.jsp").forward(request, response);
	}




}
