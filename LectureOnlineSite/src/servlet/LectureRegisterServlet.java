package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Lecture;
import service.LectureService;
import service.logic.LectureServiceLogic;


@WebServlet("/lectureRegister.do")
public class LectureRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String userId = request.getParameter("userId");
		String name = request.getParameter("name");
		String instructor = request.getParameter("instructorNickName");
		String url = request.getParameter("url");
		String runtime = request.getParameter("runtime");
		String chapter = request.getParameter("hi");
		System.out.println(chapter);
		String price = request.getParameter("price"); //Integer.parseInt(); ���
		

		if(price == null || price.trim().equals("")){
			price = "0";
			}
		if(chapter == null || chapter.trim().equals("")){
			chapter = "0";
			}


		LectureService service = new LectureServiceLogic();
		int lectureNum=0;
		if(service.findAll() == null) {
			lectureNum = 1;
		}
		else{
			lectureNum = service.findLastIndex()+1;
		}
		
		System.out.println("����"+lectureNum);
		Lecture lecture = new Lecture(name, instructor, runtime, Integer.parseInt(price), url, lectureNum, Integer.parseInt(chapter));

		if(service.register(lecture)) {
			request.setAttribute("lecture", lecture);
			request.setAttribute("userId", userId);
		}

		request.getRequestDispatcher("/lecComplete.do").forward(request, response);	
	}


}