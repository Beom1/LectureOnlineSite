package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Instructor;
import service.InstructorService;
import service.logic.InstructorServiceLogic;

@WebServlet("/instructorManager.do")
public class InstructorManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String userId = request.getParameter("userId");
		request.setAttribute("userId", userId);
		InstructorService service = new InstructorServiceLogic();
		List<Instructor> instructors = service.findAll();
	
		request.setAttribute("userId", userId);
		request.setAttribute("instructors", instructors);
		request.getRequestDispatcher("/views/instructorManager.jsp").forward(request, response);
		
	}
}
