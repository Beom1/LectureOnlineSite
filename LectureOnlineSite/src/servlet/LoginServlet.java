package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserService;
import service.logic.UserServiceLogic;


@WebServlet("/login.do")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userId = request.getParameter("id");
		String userPassword = request.getParameter("password");
		request.setCharacterEncoding("UTF-8");
		

		UserService service = new UserServiceLogic();
		String returnPw = service.login(userId);

		if(userPassword.equals(returnPw)) {
			request.setAttribute("userId", userId);
			request.setAttribute("loginFlag", true);
			request.getRequestDispatcher("/views/main.jsp").forward(request, response);
		}else {
			request.getRequestDispatcher("/views/login.jsp").forward(request, response);
		}
				
	}

}
