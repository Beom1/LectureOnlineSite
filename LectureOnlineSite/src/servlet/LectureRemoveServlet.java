package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.LectureService;
import service.logic.LectureServiceLogic;

/**
 * Servlet implementation class LectureDeleteServlet
 */
@WebServlet("/lectureRemove.do")
public class LectureRemoveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String userId = request.getParameter("userId");
		int delete = Integer.parseInt(request.getParameter("lectureNum"));
		System.out.println(delete);
		LectureService service = new LectureServiceLogic();
		service.remove(delete);
		request.setAttribute("userId", userId);
		request.getRequestDispatcher("lectureRegister.do").forward(request, response);
		
		
		
	}


}
