package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Lecture;
import domain.Order;
import service.UserService;
import service.logic.UserServiceLogic;


@WebServlet("/orderComplete.do")
public class OrderCompleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
				String userId = request.getParameter("userId");
				System.out.println(userId);

				int money = 0;
				UserService service = new UserServiceLogic();
				
				

				
				List<Lecture> lectures = service.findBasketLecture(userId);
				for(Lecture lecture : lectures) {
					money += lecture.getLecturePrice();
				}
				for(Lecture lecture : lectures) {
					int orderNum = 0;
					List<Order> orders = service.findAllOrder();
					if(orders.size()==0) {
						orderNum = 1;
					}else {
						for(Order order: orders) {
							if(order.getOrderNum()>orderNum) {
								orderNum = order.getOrderNum();
							}
						}
					}
					System.out.println(orderNum+userId);
					Order order = new Order(orderNum+1,lecture.getLecturePrice(), lecture, userId);
					
					service.orderRegister(order);
				}			

				request.setAttribute("userId", userId);
				request.setAttribute("lectures", lectures);
				request.setAttribute("payMoney", money);
				request.getRequestDispatcher("/views/orderComplete.jsp").forward(request, response);
	}



}
