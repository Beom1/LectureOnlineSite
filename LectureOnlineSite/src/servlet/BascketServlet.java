package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Lecture;
import service.UserService;
import service.logic.UserServiceLogic;
import store.LectureComparator;


@WebServlet("/basket.do")
public class BascketServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String userId = request.getParameter("userId");
		LectureComparator comp = new LectureComparator();
		int money = 0;
		UserService service = new UserServiceLogic();
		List<Lecture> lectures = service.findBasketLecture(userId);
		for(Lecture lecture : lectures) {
			money += lecture.getLecturePrice();
		}
		Collections.sort(lectures, comp);
		
		request.setAttribute("userId", userId);
		request.setAttribute("lectures", lectures);
		request.setAttribute("payMoney", money);
		request.getRequestDispatcher("/views/basket.jsp").forward(request, response);
	}
}
