package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Lecture;
import service.LectureService;
import service.logic.LectureServiceLogic;


@WebServlet("/lectureModify.do")
public class LectureModifyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String lectureNum = request.getParameter("lectureNum");
		String userId = request.getParameter("userId");
		request.setAttribute("userId", userId);
		if(lectureNum == null || lectureNum.trim().equals("")){
			lectureNum = "0";
			}
		
		LectureService service = new LectureServiceLogic();
		Lecture modifyLecture = service.find(Integer.parseInt(lectureNum));
		
		request.setAttribute("lecture", modifyLecture);

		
		request.getRequestDispatcher("/views/lectureModify.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		String userId = request.getParameter("userId");
		request.setAttribute("userId", userId);
		String num = request.getParameter("lectureNum");
		String name = request.getParameter("name");
		String instructorNickName = request.getParameter("instructorNickName");
		String url = request.getParameter("uri");
		String runtime = request.getParameter("runtime");
		String chapter = request.getParameter("chapter");
		String price = request.getParameter("price");
		
		if(num == null || num.trim().equals("")){
			num = "0";
			}
		if(price == null || price.trim().equals("")){
			price = "0";
			}
		if(chapter == null || chapter.trim().equals("")){
			chapter = "0";
			}

		
		LectureService service = new LectureServiceLogic();
	
		Lecture modifyLecture = new Lecture(name,instructorNickName, runtime, Integer.parseInt(price)
				, url, Integer.parseInt(num), Integer.parseInt(chapter));
		
		boolean result = service.modify(modifyLecture); //값에 따라 다른행동하기
		

		request.setAttribute("lecture", modifyLecture);

		request.getRequestDispatcher("/views/main.jsp").forward(request, response);
//		}
	
	}
}
