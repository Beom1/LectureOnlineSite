package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.InstructorService;
import service.logic.InstructorServiceLogic;

@WebServlet("/instructorRemove.do")
public class InstructorRemoveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			String instructorNickName = request.getParameter("instructorNickName");
			String userId = request.getParameter("userId");
			request.setAttribute("userId", userId);
			InstructorService service = new InstructorServiceLogic();
			
			service.remove(instructorNickName);
			response.sendRedirect(request.getContextPath()+"/instructorManager.do");
	}

}
