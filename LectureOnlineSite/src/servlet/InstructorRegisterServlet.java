package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Instructor;
import service.InstructorService;
import service.logic.InstructorServiceLogic;

@WebServlet("/instructorRegister.do")
public class InstructorRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String userId = request.getParameter("userId");
		request.setAttribute("userId", userId);
		InstructorService service = new InstructorServiceLogic();
	
		Instructor instructor = new Instructor();
		
		//넘어온 값 강사에 셋팅
		instructor.setInstructorNickName(request.getParameter("instructorNickName"));
		instructor.setInstructorName(request.getParameter("instructorName"));
		instructor.setInstructorProfile( request.getParameter("instructorProfile"));
		

		//서비스에 instructor등록하고 , 응답하고 응답할때 jsp주소 다시보내
		service.register(instructor);
		request.getRequestDispatcher("/views/main.jsp?userId="+userId).forward(request, response);
//		response.sendRedirect(request.getContextPath()+"/instructorManager.do");
	}

}
