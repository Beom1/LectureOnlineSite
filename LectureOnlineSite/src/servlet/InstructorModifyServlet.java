package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Instructor;
import service.InstructorService;
import service.logic.InstructorServiceLogic;

@WebServlet("/instructorModify.do")
public class InstructorModifyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String instructorNickName = request.getParameter("name");
		String userId = request.getParameter("userId");

		System.out.println(instructorNickName);
		
		InstructorService service = new InstructorServiceLogic();
		Instructor instructorModify = service.find(instructorNickName);
		
		request.setAttribute("instructor", instructorModify);
		request.setAttribute("userId", userId);
		request.getRequestDispatcher("/views/instructorModify.jsp").forward(request, response);
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String instructorNickName = request.getParameter("instructorNickName");
		String instructorName = request.getParameter("instructorName");
		String instructorProfile = request.getParameter("instructorProfile");
		String userId = request.getParameter("userId");
		
		Instructor instructor = new Instructor(instructorNickName, instructorName, instructorProfile);
		InstructorService service = new InstructorServiceLogic();
//		instructor.setInstructorNickName(instructorNickName);
		
		System.out.println(instructor.getInstructorNickName());
		System.out.println(instructor.getInstructorName());
		System.out.println(instructor.getInstructorProfile());
		
		request.setAttribute("userId", userId);
		service.modify(instructor);
		request.getRequestDispatcher(request.getContextPath()+"/instructorManager.do");
	
	}

}
