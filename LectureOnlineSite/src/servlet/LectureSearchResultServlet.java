package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Lecture;
import service.LectureService;
import service.logic.LectureServiceLogic;
import store.LectureComparator;

/**
 * Servlet implementation class LectureSearchResultServlet
 */
@WebServlet("/lectureResult.do")
public class LectureSearchResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String category = request.getParameter("category"); //강사닉네임,강의명,최신순
		String search = request.getParameter("search");	
		String userId = request.getParameter("userId");
		request.setAttribute("userId", userId);
		boolean flag = Boolean.parseBoolean(request.getParameter("flag"));
		System.out.println("category = "+category);
		System.out.println("search = "+search);
		
		LectureService service = new LectureServiceLogic();
		List<Lecture> resultLectures = new ArrayList<>();
		List<Lecture> allLectures = service.findAll();
		LectureComparator comp = new LectureComparator();
		Collections.sort(allLectures, comp);
		
		if(category.equals("lectureName")) {
			for(Lecture lecture : allLectures) {
				if(lecture.getLectureName().equals(search)) {
					resultLectures.add(lecture);
					
				}
			}
		}
		else if(category.equals("instructorName")) {
			for(Lecture lecture : allLectures) {
				if(lecture.getInstructorNickName().equals(search)) {
					resultLectures.add(lecture);
				}
			}
		}
		else if(category.equals("lastest")) {
				resultLectures.addAll(allLectures);
		}
		
		
		if(flag) {
			request.setAttribute("lectures", resultLectures);
			request.getRequestDispatcher("/views/lectureSearch.jsp").forward(request, response);
		}
		else {
		request.setAttribute("lectures", resultLectures);
		request.getRequestDispatcher("/views/lectureManage.jsp").forward(request, response);
		
		}
		
	}



}
