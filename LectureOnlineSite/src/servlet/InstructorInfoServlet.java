package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Instructor;
import service.InstructorService;
import service.logic.InstructorServiceLogic;


@WebServlet("/instructorInfo.do")
public class InstructorInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String instructorNickName = request.getParameter("instructorNickName");
//		String instructorNickName = "��������";
		String userId = request.getParameter("userId");
		
		InstructorService service = new InstructorServiceLogic();
		Instructor instructor = service.find(instructorNickName);
		
		System.out.println(instructor);

		request.setAttribute("userId", userId);
		request.setAttribute("instructor", instructor);
		request.getRequestDispatcher("/views/instructorInfo.jsp").forward(request, response);
	}

}
