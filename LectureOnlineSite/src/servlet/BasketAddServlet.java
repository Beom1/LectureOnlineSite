package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.LectureService;
import service.logic.LectureServiceLogic;


@WebServlet("/basketAdd.do")
public class BasketAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int lectureNum = Integer.parseInt(request.getParameter("lectureNum"));
		String userId = request.getParameter("userId");
		LectureService service = new LectureServiceLogic();
		
		boolean success = service.basketAdd(lectureNum, userId);
		if(!success)
			System.out.println("중복된다오");
		request.setAttribute("userId", userId);
		
		request.getRequestDispatcher("/lectureSearch.do").forward(request, response);		
	}

}
