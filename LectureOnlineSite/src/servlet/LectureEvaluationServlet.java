package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Evaluation;
import service.LectureService;
import service.logic.LectureServiceLogic;

@WebServlet("/evaluation.do")
public class LectureEvaluationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String content = request.getParameter("inputstr");
		String score = request.getParameter("star-input");
		String userId = request.getParameter("userId");
		String lectureNum = request.getParameter("lectureNum");
		System.out.println(content+score);
		if(score == null || score.trim().equals("")){
			score = "0";
			}
		if(lectureNum == null || lectureNum.trim().equals("")){
			lectureNum = "0";
			}

		
		LectureService service = new LectureServiceLogic();
		int evaluationNum = 0;
		if(service.findAllEvaluation()==null) {
			evaluationNum = 1;
		}else {
			evaluationNum = service.findEvaluationLastIndex()+1;
		}
		System.out.println(userId+lectureNum);
		request.setAttribute("userId",userId);
		request.setAttribute("lectureNum", lectureNum);
		Evaluation evaluation = new Evaluation(evaluationNum, content, Integer.parseInt(score), userId, Integer.parseInt(lectureNum));
		System.out.println(evaluation);
		//유저정보(로그인), 강의정보(마이페이지) 필요함.
		boolean success = service.Evaluate(evaluation);
		System.out.println(success);
		request.getRequestDispatcher("/views/main.jsp").forward(request, response);
		
	}

}
