package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Lecture;
import domain.User;
import service.LectureService;
import service.UserService;
import service.logic.LectureServiceLogic;
import service.logic.UserServiceLogic;


@WebServlet("/myPage.do")
public class MyPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String userId = request.getParameter("userId");
		UserService service = new UserServiceLogic();
		User user = service.find(userId);
		request.setAttribute("user", user);
		LectureService lectureService = new LectureServiceLogic();
//		String lectureNum = request.getParameter("lecture_num");
//		List<Lecture> lectures = service.findAllLecture();	
//		request.setAttribute("lectures",lectures);
//		System.out.println("ok");
		
		List<Lecture> lectures = lectureService.findAll();
		request.setAttribute("userId",userId);
		request.setAttribute("lectures", lectures);
		request.getRequestDispatcher("/views/myPage.jsp").forward(request, response);
	}
}
