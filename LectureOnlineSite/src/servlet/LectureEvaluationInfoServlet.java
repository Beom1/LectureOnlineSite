package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Evaluation;
import domain.Lecture;
import service.LectureService;
import service.logic.LectureServiceLogic;


@WebServlet("/evaluationInfo.do")
public class LectureEvaluationInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String evaluationNum = request.getParameter("evaluationNum");
		System.out.println(evaluationNum);
		if(evaluationNum == null || evaluationNum.trim().equals("")){
			evaluationNum = "0";
			}
		LectureService service = new LectureServiceLogic();
		Evaluation evaluation = service.findEvaluation(Integer.parseInt(evaluationNum));
		System.out.println(evaluation);
		Lecture lecture = service.find(evaluation.getLectureNum());
		String lectureName = lecture.getLectureName();
		request.setAttribute("evaluation", evaluation);
		request.setAttribute("lectureName", lectureName);
		
		request.getRequestDispatcher("/views/lectureEvaluationDetail.jsp").forward(request, response);
			
	}


}
