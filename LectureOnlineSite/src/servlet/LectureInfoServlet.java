package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Evaluation;
import domain.Lecture;
import service.LectureService;
import service.logic.LectureServiceLogic;


@WebServlet("/lectureInfo.do")
public class LectureInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean flag = Boolean.parseBoolean(request.getParameter("flag"));
		String lectureNum = request.getParameter("lectureNum");
		LectureService service = new LectureServiceLogic();
		String userId = request.getParameter("userId");
		if(lectureNum == null || lectureNum.trim().equals("")){
			lectureNum = "0";
			}
		
		List<Evaluation> evaluations = service.findList(Integer.parseInt(lectureNum));
		Lecture detailLecture = service.find(Integer.parseInt(lectureNum));

		request.setAttribute("userId", userId);
		request.setAttribute("lecture", detailLecture);
		request.setAttribute("evaluations", evaluations);
		request.setAttribute("flag", flag);
		request.getRequestDispatcher("/views/lectureInfo.jsp").forward(request, response);
		
	}


}
