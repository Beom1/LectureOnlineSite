package store;

import java.util.List;

import domain.Lecture;

public interface LectureStore {
	
	public boolean create(Lecture lecture);
	public Lecture retrieve(int lectureNum);
	public List<Lecture> retrieveAll();
	public boolean update(Lecture lecture);
	public boolean delete(int lectureNum);
	public boolean basketAdd(int lectureNum, String userId);
	public List<Lecture> myBasket(String userId);
}
