package store;

import java.util.List;

import domain.Instructor;

public interface InstructorStore {
	public boolean create(Instructor instructor);
	public Instructor retrieve(String instructorNickName);
	public List<Instructor> retrieveAll();
//	public boolean update(String instructorNickName, String instructorName, String instructorProfile);
	public boolean update(Instructor instructor);
	public boolean delete(String instructorNickName);
//	public List<Instructor> readByNick(String nickName);
}
