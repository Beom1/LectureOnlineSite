package store;

import java.util.List;

import domain.Evaluation;

public interface EvaluationStore {
	public boolean create(Evaluation evaluation);
	public Evaluation retrieve(int evaluationNum);
	public List<Evaluation> retrieveList(int lectureNum);
	public List<Evaluation> retrieveAll();
	
}
