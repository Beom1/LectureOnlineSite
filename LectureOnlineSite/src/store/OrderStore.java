package store;

import java.util.List;

import domain.Order;

public interface OrderStore {
	
	List<Order> retrieveAll();
	boolean create(Order order);
	
	
}
