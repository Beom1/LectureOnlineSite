package store;

import java.util.List;

import domain.Lecture;
import domain.User;

public interface UserStore {
	boolean create(User user);
	String pwReturn(String userId);
	User retrieve(String userId);
	List<User> retrieveAll();
	boolean delete(String userId);
	boolean update(User user);

}
