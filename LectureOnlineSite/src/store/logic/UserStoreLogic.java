package store.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.User;
import store.ConnectionFactory;
import store.JdbcUtils;
import store.UserStore;

public class UserStoreLogic implements UserStore {
	private ConnectionFactory factory;

	public UserStoreLogic() {
		this.factory = ConnectionFactory.getInstance();
	}

	@Override
	public boolean create(User user) {
		String sql = "insert into User_tb(user_id, user_name, user_pw, user_email, user_birth, user_phone) values(?, ?, ?, ?, ?, ?)";

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, user.getUserId());
			pstmt.setString(2, user.getUserName());
			pstmt.setString(3, user.getUserPassword());
			pstmt.setString(4, user.getUserEmail());
			pstmt.setString(5, user.getUserBirth());
			pstmt.setString(6, user.getUserPhoneNum());
		
			int num = pstmt.executeUpdate();
			
			if (num == 1) {
				System.out.println("회원가입 성공");
				return true;
			} else {
				System.out.println("회원가입 실패");		
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, pstmt);
		}
		return false;
	}

	@Override
	public User retrieve(String userId) {
		String sql = "Select user_id, user_pw, user_name,  user_email, user_birth, user_phone"
				+ " from User_tb"
				+ " where user_id = ?";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		User user = null;
		
		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userId);
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				user = new User();
				user.setUserId(rs.getString("user_id"));
				user.setUserName(rs.getString("user_name"));
				user.setUserPassword(rs.getString("user_pw"));
				user.setUserEmail(rs.getString("user_email"));
				user.setUserBirth(rs.getString("user_birth"));
				user.setUserPhoneNum(rs.getString("user_phone"));
			}		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, pstmt, rs);
		}	
		return user;
	}
	
	@Override
	public List<User> retrieveAll() {
		String sql = "select  * from user_tb";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		List<User> users = new ArrayList<>();
		
		try {
			conn = factory.createConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				User user = new User();
				user.setUserId(rs.getString("user_id"));
				user.setUserName(rs.getString("user_name"));
				user.setUserPassword(rs.getString("user_pw"));
				user.setUserEmail(rs.getString("user_email"));
				user.setUserBirth(rs.getString("user_birth"));
				user.setUserPhoneNum(rs.getString("user_phone"));
				users.add(user);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn, stmt, rs);
		}		
		return users;
	}

	@Override
	public boolean delete(String userId) {
		Connection conn = null;
		Statement stmt = null;
		
		String sql = "delete from User_tb where id= " + userId;
		try {
			conn = factory.createConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);		
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, stmt);
		}
		return false;
	}
	
	@Override
	public boolean update(User user) {
		String sql = "update user_tb set user_pw=?, user_name=?, user_email=?, user_birth=?, user_phone=?"
				+ " where user_id = ?";		
		Connection conn = null;
		PreparedStatement pstmt = null;
		int result =0;
	
		try {
			conn=factory.createConnection();
			pstmt =conn.prepareStatement(sql);
			pstmt.setString(1, user.getUserPassword());
			pstmt.setString(2, user.getUserName());
			pstmt.setString(3, user.getUserEmail());
			pstmt.setString(4, user.getUserBirth());
			pstmt.setString(5, user.getUserPhoneNum());
			pstmt.setString(6, user.getUserId());
			result = pstmt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn,pstmt);
		}
		return result >0;
	}

	@Override
	public String pwReturn(String userId) {
		// TODO Auto-generated method stub
		String sql = "Select user_pw"
				+ " from User_tb"
				+ " where user_id = ?";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String password = null;
		
		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userId);
			rs = pstmt.executeQuery();
			if(rs.next()) {
			password = rs.getString("user_pw");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, pstmt, rs);
		}	
		return password;
	
	}


	
}
