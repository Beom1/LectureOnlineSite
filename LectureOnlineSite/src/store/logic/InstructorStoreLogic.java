package store.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.Instructor;
import store.ConnectionFactory;
import store.InstructorStore;
import store.JdbcUtils;

public class InstructorStoreLogic implements InstructorStore{
//	update����!
	private ConnectionFactory factory;
	
	public InstructorStoreLogic() {
		this.factory = ConnectionFactory.getInstance();
	}
	
	@Override
	public boolean create(Instructor instructor) {
		//connectionȹ�� 
		Connection conn = null;
		//sql �����غ�
		PreparedStatement pstmt = null;
		//���� ����
		String sql = "INSERT INTO instructor_tb (instructor_nickname,"
				+" instructor_name, instructor_profile)"
				+" VALUES(?,?,?)";
		
		try {
			conn=factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, instructor.getInstructorNickName());
			pstmt.setString(2, instructor.getInstructorName());
			pstmt.setString(3, instructor.getInstructorProfile());
			
			int num = pstmt.executeUpdate();
			if(num == 1) {
				System.out.println("instructor Create Success!");
				return true;
			} else {
				System.out.println("instructor Create Fail!");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn, pstmt);		//JDBCUtils�� �ִ°� ����ؼ� �ݾ���
		}
		return false;
	}

	@Override
	public Instructor retrieve(String instructorNickName) {
		String sql = "Select instructor_nickname, instructor_name, instructor_profile"
				+" from instructor_tb"
				+" where instructor_nickname = ?";
		
		Connection conn= null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			conn=factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, instructorNickName);
			rs=pstmt.executeQuery();
			if(rs.next()) {
				Instructor instructor = new Instructor();
				instructor.setInstructorNickName(instructorNickName);
				instructor.setInstructorName(rs.getString("instructor_name"));
				instructor.setInstructorProfile(rs.getString("instructor_profile"));
				
				return instructor;
			}
		} catch (SQLException e) {	
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn, pstmt, rs);
		}

		
		return null;
	}

	@Override
	public List<Instructor> retrieveAll() {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs =null;
		List<Instructor> list = new ArrayList<>();
		
		String sql = "select instructor_nickname, instructor_name, instructor_profile"
				+" from instructor_tb";
		
		try {
			conn=factory.createConnection();
			stmt = conn.createStatement();
			rs=stmt.executeQuery(sql);
			
			while(rs.next()) {
				Instructor instructor = new Instructor();
				instructor.setInstructorNickName(rs.getString("instructor_nickname"));
				instructor.setInstructorName(rs.getString("instructor_name"));
				instructor.setInstructorProfile(rs.getString("instructor_profile"));
				
				list.add(instructor);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn, stmt, rs);
		}
		
		return list;
	}

	@Override
	public boolean update(Instructor instructor) {
		Connection conn = null;
		PreparedStatement pstmt = null;
//		ResultSet rs = null;
		
		String sql = "UPDATE instructor_tb "
				+"SET instructor_name= ?, instructor_profile= ?"
				+" WHERE instructor_nickname= ?";
		
		try {
			conn=factory.createConnection();
			pstmt=conn.prepareStatement(sql);		
			
//			pstmt.setString(1, instructorName);
//			pstmt.setString(2, instructorProfile);
//			pstmt.setString(3, instructorNickName);
			
			pstmt.setString(1, instructor.getInstructorName());
			pstmt.setString(2, instructor.getInstructorProfile());
			pstmt.setString(3, instructor.getInstructorNickName());
			
			int num = pstmt.executeUpdate();
		
			if(num==1) {
				System.out.println("instructor update success!");
				return true;
			}
			else {
				System.out.println("instructor update fail!");
			}			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn, pstmt);
		}	
		return false;
	}

	@Override
	public boolean delete(String instructorNickName) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String sql = "Delete from instructor_tb"
				+" where instructor_nickname = ?";
		
		try {
			conn=factory.createConnection();
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, instructorNickName);
//			rs=pstmt.executeQuery();
			int num = pstmt.executeUpdate();
			if(num == 1) {
				System.out.println("instructor delete success!");
				return true;
			}
			else {
				System.out.println("instructor delete fail");
			}
			
					
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn, pstmt);
		}
		return false;
	}

//	@Override
//	public List<Instructor> readByNick(String nickName) {
//		Connection conn =null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//		
//		String sql = "Select instructor_nickname, instructor_name, instructor_profile"
//				+" from instructor_tb"
//				+" where instructor_nickname like ?";
//		
//		List<Instructor> instructors = new ArrayList<>();
//		
//		try {
//			conn=factory.createConnection();
//			pstmt = conn.prepareStatement(sql);
//			pstmt.setString(1, "%"+nickName+"%");
//			rs=pstmt.executeQuery();
//			while(rs.next()) {
//				Instructor instructor = new Instructor();
//				instructor.setInstructorNickName(rs.getString("instructor_nickname"));
//				instructor.setInstructorName(rs.getString("instructor_name"));
//				instructor.setInstructorProfile(rs.getString("instructorProfile"));
//				
//				instructors.add(instructor);
//				
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}finally {
//			JdbcUtils.close(conn, pstmt, rs);
//		}
//		return instructors;
//	}

}
