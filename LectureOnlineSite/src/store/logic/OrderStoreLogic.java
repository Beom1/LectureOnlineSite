package store.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Lecture;
import domain.Order;
import service.LectureService;
import service.logic.LectureServiceLogic;
import store.ConnectionFactory;
import store.JdbcUtils;
import store.OrderStore;

public class OrderStoreLogic implements OrderStore {
	
	private ConnectionFactory factory;
	
	public OrderStoreLogic() {
		this.factory=ConnectionFactory.getInstance();
	}

	@Override
	public List<Order> retrieveAll() {
		String sql = "select order_num, order_amount, user_id, lecture_num, order_date" +" from Order_tb";
	
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		List<Order> orders = new ArrayList<>();
		
		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				LectureService service = new LectureServiceLogic();
				Lecture lecture = service.find(rs.getInt("lecture_num"));
				
				Order order = new Order();
				order.setOrderNum(rs.getInt("order_num"));
				order.setOrderAmount(rs.getInt("order_amount"));
				order.setOrderDate(rs.getDate("order_date"));
				order.setOrderLecture(lecture);
				order.setOrderUser(rs.getString("user_id"));			
				orders.add(order);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, pstmt, rs);
		}
		return orders;
	}

	@Override
	public boolean create(Order order) {
		String sql = "insert into order_tb (order_num, order_amount, user_id, lecture_num, order_date)" +" values(?,?,?,?,?)";
		
		Connection conn = null;
		PreparedStatement pstmt = null;	
		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, order.getOrderNum());
			pstmt.setInt(2, order.getOrderAmount());
			pstmt.setString(3, order.getOrderUser());
			pstmt.setInt(4, order.getOrderLecture().getLectureNum());
			pstmt.setDate(5, new java.sql.Date(new java.util.Date().getTime()));
			
			int result = pstmt.executeUpdate();
			if(result>0) {
				return true;
			}		
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn,pstmt);
		}
		return false;
	}
}
