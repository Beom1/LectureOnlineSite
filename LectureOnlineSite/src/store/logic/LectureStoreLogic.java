package store.logic;

import java.sql.Connection;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import domain.Lecture;
import store.ConnectionFactory;
import store.JdbcUtils;
import store.LectureStore;

public class LectureStoreLogic implements LectureStore {

	private ConnectionFactory factory;

	public LectureStoreLogic() {
		this.factory = ConnectionFactory.getInstance();
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean create(Lecture lecture) {
		// TODO Auto-generated method stub
		String sql = "insert into lecture_tb (lecture_num, instructor_nickname, lecture_name,"
				+ " lecture_video, lecture_uploaddate, lecture_chapter, lecture_runtime, lecture_price)"
				+ " values(?,?,?,?,?,?,?,?)";
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, lecture.getLectureNum());
			pstmt.setString(2, lecture.getInstructorNickName());
			pstmt.setString(3, lecture.getLectureName());
			pstmt.setString(4, lecture.getLectureVideoUrl());
			pstmt.setDate(5, new java.sql.Date(new java.util.Date().getTime()));
			pstmt.setInt(6, lecture.getLectureChapter());
			pstmt.setString(7, lecture.getLectureRuntime());
			pstmt.setInt(8, lecture.getLecturePrice());
			int result = pstmt.executeUpdate();
			if (result > 0) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, pstmt);
		}
		return false;
	}

	@Override
	public Lecture retrieve(int lectureNum) {
		// TODO Auto-generated method stub
		String sql = "select * from lecture_tb" + " where lecture_num = ?";

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Lecture lecture = null;

		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, lectureNum);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				lecture = new Lecture();
				lecture.setLectureNum(rs.getInt("lecture_num"));
				lecture.setInstructorNickName(rs.getString("instructor_nickname"));
				lecture.setLectureName(rs.getString("lecture_name"));
				lecture.setLectureVideoUrl(rs.getString("lecture_video"));
				lecture.setLectureUploadDate(rs.getDate("lecture_uploaddate"));
				lecture.setLectureChapter(rs.getInt("lecture_chapter"));
				lecture.setLectureRuntime(rs.getString("lecture_runtime"));
				lecture.setLecturePrice(rs.getInt("lecture_price"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, pstmt, rs);
		}

		return lecture;
	}

	@Override
	public List<Lecture> retrieveAll() {
		// TODO Auto-generated method stub
		String sql = "select * from lecture_tb";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<Lecture> lectures = new ArrayList<>();

		try {
			conn = factory.createConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Lecture lecture = new Lecture();
				lecture.setLectureNum(rs.getInt("lecture_num"));
				lecture.setInstructorNickName(rs.getString("instructor_nickname"));
				lecture.setLectureName(rs.getString("lecture_name"));
				lecture.setLectureVideoUrl(rs.getString("lecture_video"));
				lecture.setLectureUploadDate(rs.getDate("lecture_uploaddate"));
				lecture.setLectureChapter(rs.getInt("lecture_chapter"));
				lecture.setLectureRuntime(rs.getString("lecture_runtime"));
				lecture.setLecturePrice(rs.getInt("lecture_price"));

				lectures.add(lecture);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, stmt, rs);
		}

		return lectures;
	}

	@Override
	public boolean update(Lecture lecture) {
		// TODO Auto-generated method stub
		String sql = "update lecture_tb " + "set instructor_nickname=?, lecture_name=?, lecture_video=?, "
				+ "lecture_uploaddate=?, lecture_chapter=?, lecture_runtime=?, lecture_price=? "
				+ "where lecture_num = ?";
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, lecture.getInstructorNickName());
			pstmt.setString(2, lecture.getLectureName());
			pstmt.setString(3, lecture.getLectureVideoUrl());
			pstmt.setDate(4, new java.sql.Date(new java.util.Date().getTime()));
			pstmt.setInt(5, lecture.getLectureChapter());
			pstmt.setString(6, lecture.getLectureRuntime());
			pstmt.setInt(7, lecture.getLecturePrice());
			pstmt.setInt(8, lecture.getLectureNum());
			int result = pstmt.executeUpdate();
			if (result > 0) {
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, pstmt);
		}
		return false;

	}

	@Override
	public boolean delete(int lectureNum) {
		// TODO Auto-generated method stub
		String sql = "delete from lecture_tb" + " where lecture_num = ?";
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, lectureNum);
			int result = pstmt.executeUpdate();
			if (result > 0) {
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, pstmt);
		}
		return false;

	}

	@Override
	public boolean basketAdd(int lectureNum, String userId) {
		// TODO Auto-generated method stub
		String sql = "insert into userownlecture_tb (user_id, lecture_num, purchased_flag)" + " values(?,?,?)";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement("select * from userownlecture_tb");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				int a = rs.getInt("lecture_num");
				if (a == lectureNum)
					return false;
			}
			pstmt = null;
			rs = null;

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userId);
			pstmt.setInt(2, lectureNum);
			pstmt.setInt(3, 3);
			int result = pstmt.executeUpdate();
			if (result > 0) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, pstmt);
		}
		return false;
	}

	@SuppressWarnings("resource")
	@Override
	public List<Lecture> myBasket(String userId) {
		// TODO Auto-generated method stub
		String sql = "select * from userownlecture_tb where user_id=? and purchased_flag=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Integer> lectureNums = new ArrayList<>();
		List<Lecture> lectures = new ArrayList<>();
		List<Lecture> mylectures = new ArrayList<>();

		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement("select * from lecture_tb");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Lecture lecture = new Lecture();
				lecture.setLectureNum(rs.getInt("lecture_num"));
				lecture.setInstructorNickName(rs.getString("instructor_nickname"));
				lecture.setLectureName(rs.getString("lecture_name"));
				lecture.setLectureVideoUrl(rs.getString("lecture_video"));
				lecture.setLectureUploadDate(rs.getDate("lecture_uploaddate"));
				lecture.setLectureChapter(rs.getInt("lecture_chapter"));
				lecture.setLectureRuntime(rs.getString("lecture_runtime"));
				lecture.setLecturePrice(rs.getInt("lecture_price"));

				lectures.add(lecture);
			}
			pstmt = null;
			rs = null;
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userId);
			pstmt.setInt(2, 3);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				if (rs.getString("user_id").equals(userId)) {
					lectureNums.add(rs.getInt("lecture_num"));
				}
			}
			for(int i = 0; i < lectureNums.size(); i ++) {
				
				for(Lecture lecture : lectures) {
					if(lecture.getLectureNum()==lectureNums.get(i)) {
						mylectures.add(lecture);
					}
				}
			}
		}

		catch (

		SQLException e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, pstmt);
		}
		return mylectures;
	}
}
