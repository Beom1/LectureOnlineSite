package store.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.Evaluation;
import store.ConnectionFactory;
import store.EvaluationStore;
import store.JdbcUtils;

public class EvaluationStoreLogic implements EvaluationStore{
	private ConnectionFactory factory;
	
	public EvaluationStoreLogic() {
		this.factory = ConnectionFactory.getInstance();
	}
	
	@Override
	public boolean create(Evaluation evaluation) {
		// TODO Auto-generated method stub
		String sql = "insert into evaluation_tb (evaluation_num, lecture_num, user_id, "
				+ "evaluation_contents, evaluation_score, evaluation_uploaddate) "
				+ "values(?, ?, ?, ?, ?, ?)";
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, evaluation.getEvaluationNum());
			pstmt.setInt(2, evaluation.getLectureNum());
			pstmt.setString(3, evaluation.getEvaluationUser());
			pstmt.setString(4, evaluation.getEvaluationContent());
			pstmt.setInt(5, evaluation.getEvaluationScore());
			pstmt.setDate(6, new java.sql.Date(new java.util.Date().getTime()));
			int result = pstmt.executeUpdate();
			
			if(result > 0) {
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn, pstmt);
		}
		
		return false;
		
		
	}

	@Override
	public Evaluation retrieve(int evaluationNum) {
		// TODO Auto-generated method stub
		
		String sql = "select * from evaluation_tb where evaluation_num = ?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Evaluation evaluation = null;
		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, evaluationNum);
			rs = pstmt.executeQuery();

			if(rs.next()) {
				evaluation = new Evaluation();
				evaluation.setEvaluationNum(rs.getInt("evaluation_num"));
				evaluation.setLectureNum(rs.getInt("lecture_num"));
				evaluation.setEvaluationUser(rs.getString("user_id"));
				evaluation.setEvaluationContent(rs.getString("evaluation_contents"));
				evaluation.setEvaluationScore(rs.getInt("evaluation_score"));
				evaluation.setEvaluationUploadDate(rs.getDate("evaluation_uploaddate"));
			}	
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn,pstmt,rs);
		}
		
		return evaluation;
	}

	@Override
	public List<Evaluation> retrieveAll() {
		// TODO Auto-generated method stub
		String sql = "select * from evaluation_tb";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<Evaluation> evaluations = new ArrayList<>();
		
		
		try {
			conn = factory.createConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				Evaluation evaluation = new Evaluation();
				evaluation.setEvaluationNum(rs.getInt("evaluation_num"));
				evaluation.setLectureNum(rs.getInt("lecture_num"));
				evaluation.setEvaluationUser(rs.getString("user_id"));
				evaluation.setEvaluationContent(rs.getString("evaluation_contents"));
				evaluation.setEvaluationScore(rs.getInt("evaluation_score"));
				evaluation.setEvaluationUploadDate(rs.getDate("evaluation_uploaddate"));
				
				evaluations.add(evaluation);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			JdbcUtils.close(conn, stmt, rs);
		}


		return evaluations;
	}

	@Override
	public List<Evaluation> retrieveList(int lectureNum) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				String sql = "select * from evaluation_tb where lecture_num = ?";
				Connection conn = null;
				PreparedStatement pstmt = null;
				ResultSet rs = null;
				List<Evaluation> evaluations = new ArrayList<>();
				
				
				try {
					conn = factory.createConnection();
					pstmt = conn.prepareStatement(sql);
					pstmt.setInt(1, lectureNum);
					rs = pstmt.executeQuery();
					
					while(rs.next()) {
						Evaluation evaluation = new Evaluation();
						evaluation.setEvaluationNum(rs.getInt("evaluation_num"));
						evaluation.setLectureNum(rs.getInt("lecture_num"));
						evaluation.setEvaluationUser(rs.getString("user_id"));
						evaluation.setEvaluationContent(rs.getString("evaluation_contents"));
						evaluation.setEvaluationScore(rs.getInt("evaluation_score"));
						evaluation.setEvaluationUploadDate(rs.getDate("evaluation_uploaddate"));
						
						evaluations.add(evaluation);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally {
					JdbcUtils.close(conn, pstmt, rs);
				}


				return evaluations;
	}

}
