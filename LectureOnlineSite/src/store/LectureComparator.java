package store;
import java.util.Comparator;

import domain.Lecture;

public class LectureComparator implements Comparator<Lecture> {

	@Override
	public int compare(Lecture first, Lecture second) {
		// TODO Auto-generated method stub
		double firstValue = (double)first.getLectureNum();  
        double secondValue = (double)second.getLectureNum();  
        
        if (firstValue > secondValue) {  
            return -1;  
        } else if (firstValue < secondValue) {  
            return 1;  
        } else {  
            return 0;  
        }  

	}

}
