package lectureOnline.service;

import java.util.List;

import lectureOnline.domain.Lecture;
import lectureOnline.domain.Order;
import lectureOnline.domain.User;

public interface UserService {
	void register(User user);
	User find(String userId);
	List<User> findAll();
	void modify(User user);
	void leave(String userId);
	List<Order> findAllOrder();
	List<Lecture> findAllLecture();
	
}
