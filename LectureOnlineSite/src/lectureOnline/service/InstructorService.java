package lectureOnline.service;

import java.util.List;

import lectureOnline.domain.Instructor;

public interface InstructorService {
	
	void register(Instructor instructor);
	Instructor find(String instructorNickName);
	List<Instructor> findAll();
	void modify(Instructor inatructor);
	void remove(String instructorNickName);
}
