package lectureOnline.service;

import java.util.List;

import lectureOnline.domain.Order;

public interface OrderService {
	void pay();
	List<Order> complete(int orderNum);
	List<Order> salesCheck(int orderNum);
}
