package lectureOnline.service;

import java.util.List;

import lectureOnline.domain.Lecture;
import lectureOnline.domain.Order;
import lectureOnline.domain.User;
import lectureOnline.store.LectureStore;
import lectureOnline.store.LectureStoreLogic;
import lectureOnline.store.OrderStore;
import lectureOnline.store.OrderStoreLogic;
import lectureOnline.store.UserStore;
import lectureOnline.store.UserStoreLogic;

public class UserServiceLogic implements UserService {
	
	private UserStore userStore;
	private OrderStore orderStore;
	private LectureStore lectureStore;
	
	public UserServiceLogic() {
		userStore = new UserStoreLogic();
		orderStore = new OrderStoreLogic();
		lectureStore = new LectureStoreLogic();
	}
	
	@Override
	public void register(User user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public User find(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void modify(User user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void leave(String userId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Order> findAllOrder() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Lecture> findAllLecture() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
