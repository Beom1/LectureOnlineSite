package lectureOnline.service;

import java.util.List;

import lectureOnline.domain.Evaluation;
import lectureOnline.domain.Lecture;
import lectureOnline.store.EvaluationStore;
import lectureOnline.store.EvaluationStoreLogic;
import lectureOnline.store.LectureStore;
import lectureOnline.store.LectureStoreLogic;

public class LectureServiceLogic implements LectureService{
	
	private LectureStore lectureStore;
	private EvaluationStore evaluationStore;
	
	public LectureServiceLogic() {
		//
		lectureStore = new LectureStoreLogic();
		evaluationStore=new EvaluationStoreLogic();
	}


	@Override
	public void register(Lecture lecture) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Lecture find(int lectureNum) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Lecture> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void modify(Lecture lecture) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void remove(int lectureNum) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Evaluate(Evaluation evaluation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Evaluation findEvaluation(int evaluateNum) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Evaluation> findAllEvaluation() {
		// TODO Auto-generated method stub
		return null;
	}

}
