package lectureOnline.service;

import java.util.List;

import lectureOnline.domain.Evaluation;
import lectureOnline.domain.Lecture;

public interface LectureService{
	
	void register(Lecture lecture);
	Lecture find(int lectureNum);
	List<Lecture> findAll();
	void modify(Lecture lecture);
	void remove(int lectureNum);
	void Evaluate(Evaluation evaluation);
	Evaluation findEvaluation(int evaluateNum);
	List<Evaluation> findAllEvaluation();
	
}
