package lectureOnline.store;

import java.util.List;

import lectureOnline.domain.Lecture;

public interface LectureStore {
	
	public void create(Lecture lecture);
	public Lecture retrieve(int lectureNum);
	public List<Lecture> retrieveAll();
	public void update(Lecture lecture);
	public void delete(int lectureNum);
}
