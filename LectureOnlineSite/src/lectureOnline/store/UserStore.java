package lectureOnline.store;

import java.util.List;

import lectureOnline.domain.User;

public interface UserStore {
	void create(User user);
	User retrieve(String userId);
	List<User> retrieve(User user);
	void delete(String userId);
}
