package lectureOnline.store;

import java.util.List;

import lectureOnline.domain.Order;

public interface OrderStore {
	List<Order> retrieveAll();
	void create(Order order);
}
