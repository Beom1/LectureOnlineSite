package lectureOnline.store;

public class JdbcUtils {
	public static void close(AutoCloseable...autoCloseables) {	//가변 파라미터!(하나의 타입이 개수가 다르게 올 때 사용->배열로 온거다!)
		for(AutoCloseable autoCloseable : autoCloseables) {
			
			if(autoCloseable == null) {
				continue;
			}
			
			try {
				autoCloseable.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
