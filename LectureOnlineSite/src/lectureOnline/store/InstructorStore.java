package lectureOnline.store;

import java.util.List;

import lectureOnline.domain.Instructor;

public interface InstructorStore {
	public void create(Instructor instructor);
	public Instructor retrieve(String instructorNickName);
	public List<Instructor> retrieveAll();
	public void update(Instructor instructor);
	public void delete(String instructorNickName);
}
