package lectureOnline.store;

import java.util.List;

import lectureOnline.domain.Evaluation;

public interface EvaluationStore {
	public void create(Evaluation evaluation);
	public Evaluation retrieve(int evaluationNum);
	public List<Evaluation> retrieveAll();
}
