package lectureOnline.store;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import lectureOnline.domain.User;
//import store.logic.JdbcUtils;

public class UserStoreLogic implements UserStore {
	private ConnectionFactory factory;

	public UserStoreLogic() {
		this.factory = ConnectionFactory.getInstance();
	}

	@Override
	public void create(User user) {
		String sql = "insert into user_tb(userId, userName, userPassword,userEmail,userBirth,userPhoneNum) values(?, ?, ?,?, ?, ?)";

		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, user.getUserId());
			pstmt.setString(2, user.getUserName());
			pstmt.setString(3, user.getUserPassword());
			pstmt.setString(4, user.getUserEmail());
			pstmt.setString(5, user.getUserBirth());
			pstmt.setString(6, user.getUserPhoneNum());
			int num = pstmt.executeUpdate();
			
			if (num == 1) {
				System.out.println("회원가입 성공");

			} else {
				System.out.println("회원가입 실패");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, pstmt);
		}
	}

	@Override
	public User retrieve(String userId) {
		String sql = "Select userId, userName, userPassword,userEmail,userBirth,userPhoneNum"
				+ " from user_tb"
				+ " where userId = ?";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		User user = null;
		
		try {
			conn = factory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userId);
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				user = new User();
				user.setUserId(rs.getString("userId"));
				user.setUserName(rs.getString("userName"));
				user.setUserPassword(rs.getString("userPassword"));
				user.setUserEmail(rs.getString("userEmail"));
				user.setUserBirth(rs.getString("userBirth"));
				user.setUserPhoneNum(rs.getString("userPhoneNum"));
			}		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(conn, pstmt, rs);
		}	
		return user;
	}

	@Override
	public List<User> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(String userId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(User user) {
		// TODO Auto-generated method stub

	}

}
