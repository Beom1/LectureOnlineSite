package lectureOnline.domain;

public class Evaluation {
	private String evaluationContent;
	private int evaluationScore;
	private String evaluationUser;
//	private date evaluationUploadDate;
	private String evaluationUploadDate;
	private int lectureNum;
	
	public Evaluation() {
		
	}
	
	public Evaluation(String evaluationContent, int evaluationScore, String evaluationUser, String evaluationUploadDate, int lectureNum) {
		this.evaluationContent=evaluationContent;
		this.evaluationScore=evaluationScore;
		this.evaluationUser=evaluationUser;
		this.evaluationUploadDate=evaluationUploadDate;
		this.lectureNum=lectureNum;
	}

	public String getEvaluationContent() {
		return evaluationContent;
	}

	public void setEvaluationContent(String evaluationContent) {
		this.evaluationContent = evaluationContent;
	}

	public int getEvaluationScore() {
		return evaluationScore;
	}

	public void setEvaluationScore(int evaluationScore) {
		this.evaluationScore = evaluationScore;
	}

	public String getEvaluationUser() {
		return evaluationUser;
	}

	public void setEvaluationUser(String evaluationUser) {
		this.evaluationUser = evaluationUser;
	}

	public String getEvaluationUploadDate() {
		return evaluationUploadDate;
	}

	public void setEvaluationUploadDate(String evaluationUploadDate) {
		this.evaluationUploadDate = evaluationUploadDate;
	}

	public int getLectureNum() {
		return lectureNum;
	}

	public void setLectureNum(int lectureNum) {
		this.lectureNum = lectureNum;
	}
	
	
}
