package lectureOnline.domain;

public class Order {
	private String orderName;
	private int orderAmount;
	private Lecture orderLecture;
//	private date orderDate;
	private String orderDate;
	private String orderUser;
	
	public Order() {
		
	}
	
	public Order(String orderName, int orderAmount, Lecture orderLecture, String orderDate, String orderUser) {
		this.orderAmount=orderAmount;
		this.orderName=orderName;
		this.orderLecture=orderLecture;
		this.orderDate=orderDate;
		this.orderUser=orderUser;
	}
	
	

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public int getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(int orderAmount) {
		this.orderAmount = orderAmount;
	}

	public Lecture getOrderLecture() {
		return orderLecture;
	}

	public void setOrderLecture(Lecture orderLecture) {
		this.orderLecture = orderLecture;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderUser() {
		return orderUser;
	}

	public void setOrderUser(String orderUser) {
		this.orderUser = orderUser;
	}
	
	}
