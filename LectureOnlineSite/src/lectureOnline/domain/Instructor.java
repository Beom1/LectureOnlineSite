package lectureOnline.domain;

public class Instructor {
	private String instructorNickName;
	private String instructorName;
	private String instructorProfile;
	
	
	public Instructor() {
		
	}
	
	public Instructor(String instructorNickName, String instructorName, String instructorProfile) {
		this.instructorName=instructorName;
		this.instructorNickName=instructorNickName;
		this.instructorProfile=instructorProfile;
	}
	
	public String getInstructorNickName() {
		return instructorNickName;
	}
	public void setInstructorNickName(String instructorNickName) {
		this.instructorNickName = instructorNickName;
	}
	public String getInstructorName() {
		return instructorName;
	}
	public void setInstructorName(String instructorName) {
		this.instructorName = instructorName;
	}
	public String getInstructorProfile() {
		return instructorProfile;
	}
	public void setInstructorProfile(String instructorProfile) {
		this.instructorProfile = instructorProfile;
	}
	
	
	
}
