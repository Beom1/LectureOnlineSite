package domain;

public class User {
	private String userId;
	private String userName;
	private String userPassword;
	private String userEmail;
	private String userBirth;
	private String userPhoneNum;
	
	public User() {
		
	}
	
	public User(String userId, String userName, String userPassword, String userEmail, String userBirth, String userPhoneNum) {
		this.userId=userId;
		this.userName=userName;
		this.userPassword=userPassword;
		this.userEmail=userEmail;
		this.userBirth=userBirth;
		this.userPhoneNum=userPhoneNum;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserBirth() {
		return userBirth;
	}

	public void setUserBirth(String userBirth) {
		this.userBirth = userBirth;
	}

	public String getUserPhoneNum() {
		return userPhoneNum;
	}

	public void setUserPhoneNum(String userPhoneNum) {
		this.userPhoneNum = userPhoneNum;
	}
	
	
	
}
