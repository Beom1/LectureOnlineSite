package domain;

import java.sql.Date;

public class Order {
	private int orderNum;
	private int orderAmount;
	private Lecture orderLecture;
//	private date orderDate;
	private Date orderDate;
	private String orderUser;
	
	public Order() {
		
	}
	
	public Order(int orderNum, int orderAmount, Lecture orderLecture, String orderUser) {
		this.orderAmount=orderAmount;
		this.orderNum=orderNum;
		this.orderLecture=orderLecture;
		this.orderDate=null;
		this.orderUser=orderUser;
	}

	public int getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}

	public int getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(int orderAmount) {
		this.orderAmount = orderAmount;
	}

	public Lecture getOrderLecture() {
		return orderLecture;
	}

	public void setOrderLecture(Lecture orderLecture) {
		this.orderLecture = orderLecture;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderUser() {
		return orderUser;
	}

	public void setOrderUser(String orderUser) {
		this.orderUser = orderUser;
	}
	
	}
