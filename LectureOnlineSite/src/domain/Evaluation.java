package domain;

import java.util.Date;

public class Evaluation {
	private int evaluationNum;
	private String evaluationContent;
	private int evaluationScore;
	private String evaluationUser;
	private Date evaluationUploadDate;
	private int lectureNum;
	
	public Evaluation() {
		
	}
	
	public Evaluation(int evaluationNum,String evaluationContent, int evaluationScore, String evaluationUser,int lectureNum) {
		this.evaluationNum = evaluationNum;
		this.evaluationContent=evaluationContent;
		this.evaluationScore=evaluationScore;
		this.evaluationUser=evaluationUser;
		this.evaluationUploadDate=null;
		this.lectureNum=lectureNum;
	}
	public int getEvaluationNum() {
		return evaluationNum;
	}

	public void setEvaluationNum(int evaluationNum) {
		this.evaluationNum = evaluationNum;
	}

	public String getEvaluationContent() {
		return evaluationContent;
	}

	public void setEvaluationContent(String evaluationContent) {
		this.evaluationContent = evaluationContent;
	}

	public int getEvaluationScore() {
		return evaluationScore;
	}

	public void setEvaluationScore(int evaluationScore) {
		this.evaluationScore = evaluationScore;
	}

	public String getEvaluationUser() {
		return evaluationUser;
	}

	public void setEvaluationUser(String evaluationUser) {
		this.evaluationUser = evaluationUser;
	}

	public Date getEvaluationUploadDate() {
		return evaluationUploadDate;
	}

	public void setEvaluationUploadDate(Date evaluationUploadDate) {
		this.evaluationUploadDate = evaluationUploadDate;
	}

	public int getLectureNum() {
		return lectureNum;
	}

	public void setLectureNum(int lectureNum) {
		this.lectureNum = lectureNum;
	}
	
	
}
