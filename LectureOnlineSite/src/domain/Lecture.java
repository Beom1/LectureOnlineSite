package domain;

import java.util.Date;

public class Lecture {
	private String lectureName;
	private String instructorNickName;
	private Date lectureUploadDate;
	private String lectureRuntime;
	private int lectureChapter;
	private int lecturePrice;
	private String lectureVideoUrl;
	private int lectureNum;
	
	public Lecture() {
		
	}
	
	public Lecture(String lectureName, String instructorNickName, String lectureRuntime, int lecturePrice, String lectureVideoUrl, int lectureNum,  int lectureChapter) {
		
		this.lectureName=lectureName;
		this.lectureNum=lectureNum;
		this.lecturePrice=lecturePrice;
		this.lectureChapter=lectureChapter;
		this.lectureRuntime=lectureRuntime;
		this.lectureUploadDate=null;
		this.lectureVideoUrl=lectureVideoUrl;
		this.instructorNickName=instructorNickName;
	}

	public String getLectureName() {
		return lectureName;
	}
	public void setLectureName(String lectureName) {
		this.lectureName = lectureName;
	}
	public String getInstructorNickName() {
		return instructorNickName;
	}
	public void setInstructorNickName(String instructorNickName) {
		this.instructorNickName = instructorNickName;
	}
	public int getLectureChapter() {
		return lectureChapter;
	}
	public void setLectureChapter(int lectureChapter) {
		this.lectureChapter = lectureChapter;
	}
	public int getLecturePrice() {
		return lecturePrice;
	}
	public void setLecturePrice(int lecturePrice) {
		this.lecturePrice = lecturePrice;
	}
	public String getLectureVideoUrl() {
		return lectureVideoUrl;
	}
	public void setLectureVideoUrl(String lectureVideoUrl) {
		this.lectureVideoUrl = lectureVideoUrl;
	}
	public int getLectureNum() {
		return lectureNum;
	}
	public void setLectureNum(int lectureNum) {
		this.lectureNum = lectureNum;
	}

	public Date getLectureUploadDate() {
		return lectureUploadDate;
	}

	public void setLectureUploadDate(Date lectureUploadDate) {
		this.lectureUploadDate = lectureUploadDate;
	}

	public String getLectureRuntime() {
		return lectureRuntime;
	}

	public void setLectureRuntime(String lectureRuntime) {
		this.lectureRuntime = lectureRuntime;
	}
	
	
	
}
