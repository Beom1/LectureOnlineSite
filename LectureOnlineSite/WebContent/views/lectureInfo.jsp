<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath }</c:set>
<html>
<head>
<meta charset="utf-8" />
<title>LectureOnlineSite</title>
<script type="text/javascript">
	
</script>
<style type="text/css">
body, p, h1, h2, h3, h4, h5, h6, ul, cl, ol, li, dl, dt, dd, table, th,
	td, form, fieldset, legend, input, textarea, button, select {
	margin: 0;
	padding: 0
}

img, fieldset {
	border: 0
}

cl, ol {
	list-style: none
}

cl {
	float: left;
	_border: 1px;
	list-style: none
}

cl li {
	float: left;
	width: 250px;
	height: 200px;
}

.instructor_name {
	font-size: 20px;
	font-weight: bold;
	width: 120px;
	height: 30px;
	background: green;
	margin-left: 30px
}

ul {
	width: 1000px;
	height: 30px;
	background: black;
	list-style: none;
	padding-top: 20px
}

ul li {
	float: left;
	margin-left: 80px;
	font-family: dotum
}

ul li a {
	font-size: 14px;
	color: yellow;
	font-weight: bold;
	text-decoration: none
}

ul rl {
	float: right;
	margin-right: 30px
}

.white a {
	color: #fff
}

em, address {
	font-style: normal
}

a {
	text-decoration: none
}

a:hover, a:active, a:focus {
	text-decoration: none
}

#wrap {
	float: left;
	width: 100%
}

.gnb {
	float: left;
	width: 100%;
	height: 30px;
	background: #fbfbfb
}

.gnb a {
	float: left;
}

.gnb_center {
	width: 135px;
	margin: 5px auto
}

.gnb_right {
	width: 1000px;
	margin: 5px auto
}

.gnb_center ul {
	float: left;
}

.gnb_center ul li {
	float: left;
	margin-right: 10px;
}

.gnb_right ol {
	float: right;
}

.gnb_right ol li {
	float: left;
	margin-right: 10px;
}

#container {
	width: 1000px;
	margin: 0 auto
}

#header {
	float: left;
	width: 1000px;
	_border: 1px solid blue
}

#header h1 {
	float: left;
	margin-left: 400px;
	color: #000000;
	font-size: 40px
}

.menu {
	float: left;
	width: 1000px;
	height: 60px;
	background: #000000
}

.left {
	float: left;
	width: 980px;
	height: 100%;
	_border: 1px solid blue
}

.left_content1 {
	float: left;
	width: 980px;
	height: 100%;
	margin-top: 10px;
	background: #edeef0
}

.right {
	float: left;
	width: 330px;
	margin: 10px 0 0 20px
}

.right_content2, .right_content7 {
	float: left;
	width: 330px;
	height: 140px;
	margin-bottom: 10px;
	background: #f8f8f8;
	_border: 1px dotted red;
	_border-radius: 700px
}

#footer {
	float: left;
	width: 1000px;
	height: 30px;
	margin: 10px 0 30px 0;
	background: #e1e1e1
}

p {
	font-size: 20px;
}

span {
	font-size: 20px;
}

.leftmargin {
	margin-left: 50px
}
</style>
</head>
<body>
	<div id="wrap">

			<%@ include file="/views/title.jspf" %>
			<div
				style="font-size: 40px; margin: 50px; margin-top: 1px; margin-bottom: 1px">
				강의 상세정보</div>
			<div
				style="font-size: 25px; margin: 100px; margin-top: 20px; margin-bottom: 10px">
				JSP와 서블릿</div>
			<div>
				<table>
					<td>
						<div id="lecture"
							style="border: 1px solid gold; width: 400px; height: 300px; overflow: auto; margin-left: 100px">
							
							<c:choose>
							<c:when test="${flag }" >
							</c:when>
							<c:otherwise>
							<iframe width="400px" height="300px"
								src="${lecture.lectureVideoUrl }" frameborder="0"
								allow="autoplay; encrypted-media" allowfullscreen></iframe>
							</c:otherwise>
							</c:choose>
						</div>
					</td>
					<td>
						<table>
							<colgroup>
								<col width="150">
								<col width="*">
							</colgroup>
							<tr>

								<th>강사닉네임</th>
								<td>${lecture.lectureName }</td>
							</tr>

							<tr>
								<th>챕터</th>
								<td>${lecture.lectureChapter }</td>
							</tr>

							<tr>
								<th>강의시간</th>
								<td>${lecture.lectureRuntime }</td>
							</tr>

							<tr>
								<th>금액</th>
								<td>${lecture.lecturePrice }</td>
							</tr>

						</table>
					</td>
				</table>
			</div>

			<div style="margin: 100px">
				<table>
					<colgroup>
						<col width="100">
						<col width="100">
						<col width="100">
						<col width="100">
					</colgroup>
					<tr>
						<th>아이디</th>
						<th>별점</th>
						<th>작성일</th>
						<th>강의명</th>
						<th>평가내용</th>
					</tr>
					<c:forEach items="${evaluations }" var="evaluation">
					<tr align="center">
						<td>${evaluation.evaluationUser }</td>
						<td>${evaluation.evaluationScore }</td>
						<td>${evaluation.evaluationUploadDate }</td>
						<td>${evaluation.evaluationContent }</td>
						<td><a  href="${ctx }evaluationInfo.do?evaluationNum=${evaluation.evaluationNum}"><input type="button" value="확인하기"></a></td>
					</tr>
					</c:forEach>


				</table>
			</div>
		</div>
	</div>
</body>
</html>
