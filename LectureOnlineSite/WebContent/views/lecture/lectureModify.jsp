<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>LectureOnlineSite</title>
<script type="text/javascript">
</script>
<style type="text/css">
body,p,h1,h2,h3,h4,h5,h6,ul,cl,ol,li,dl,dt,dd,table,th,td,form,fieldset,
legend,input,textarea,button,select{margin:0;padding:0}
img,fieldset{border:0}
cl,ol{list-style:none}

ul{width:1000px;height:30px;background:black;list-style:none;padding-top:20px}
ul li{float:left;margin-left:80px;font-family:dotum}
ul li a{font-size:14px;color:yellow;font-weight:bold;text-decoration:none}
ul rl{float:right;margin-right:30px}
.white a{color:#fff}
em,address{font-style:normal}
a{text-decoration:none}
a:hover,a:active,a:focus{text-decoration:none}
#wrap{float:left;width:100%}
.gnb{float:left;width:100%;height:30px;background:#fbfbfb}
.gnb a{float:left;}
.gnb_center{width:135px;margin:5px auto}
.gnb_right{width:1000px;margin:5px auto}
.gnb_center ul{float:left;}
.gnb_center ul li{float:left;margin-right:10px;}
.gnb_right cl{float:right;}
.gnb_right cl li{float:left;margin-right:10px;}
#container{width:1000px;margin:0 auto}
#header{float:left;width:1000px;_border:1px solid blue}
#header h1{float:left;margin-left:400px;color:#000000;font-size:40px}
.menu{float:left;width:1000px;height:60px;background:#000000}

.left{float:left;width:650px;_border:1px solid blue}
.left_content1{float:left;width:650px;height:200px;margin-top:10px}
.right{float:left;width:330px;margin:10px 0 0 20px}
.right_content2,.right_content7{float:left;width:330px;height:140px;margin-bottom:10px;background:#f8f8f8; _border:1px dotted red;_border-radius:700px}

#footer{float:left;width:1000px;height:30px;margin:10px 0 30px 0;background:#e1e1e1}
p{font-size:20px;}
.leftmargin{margin-left:50px}

</style>
</head>
<body>
<div id="wrap">
   
   <div id="container">
      <div id="header">
         <h1>재능 마켓</h1>
      </div>
      <div class="gnb">
         <div class="gnb_right">
            <cl>
               <li>Login</li>
               <li>/</li>
               <li>Register</li>
            </cl>
         </div><!-- gnb_center -->
      </div>
      <div class="menu">
         <ul>
            
            <li><a href="#"><p class="leftmargin">Home</p></a></li>
            <li><a href="#"><p>강의</p></a></li>
            <li><a href="#"><p>강사</p></a></li>
            <rl class="white"><a href="#">마이페이지</a></li></rl>
            <rl class="white"><a href="#">장바구니</a></li></rl>      
            
         </ul>
      </div>

      <div class="left">
         <div class="left_content1">
         	 <table style="margin-left:300px">
            <tr>
                <td>강의명</td>
                <td><input type="text" name="lectureName" /></td>
            </tr>

            <tr>
                <td>강의 번호</td>
                <td><input type="number" name="lectureNum" /></td>
            </tr>
            <tr>
                <td>강사 닉네임</td>
                <td><input type="text" name="instructorNickname" /></td>
            </tr>
            <tr>
                <td>챕터</td>
                <td><input type="text" name="lectureChapter" /></td>
            </tr>
	   	</tr>
            <tr>
                <td>강의금액</td>
                <td><input type="number" name="lecturePrice" /></td>
            </tr>
	 	</tr>
            <tr>
                <td>강의 시간</td>
                <td><input type="time" name="lectureTime" /></td>
            </tr>
		 	<tr>
                <td>강의 url</td>
                <td><input type="text" name="lectureVideoUrl" /></td>
            </tr>          
            <tr>
                <td></td>
                <td><input type="submit" name="submit" value="수정하기" />
            </tr>
        </table>
         </div>       
      </div>
      
      <div class="right">
         <div class="right_content2">
         	<td>동영상</td>
         <!-- 동영상 넣을부분~ -->
         </div>
      </div>
 
      <div id="footer">
      </div>
   </div>
</div>
</body>
</html>