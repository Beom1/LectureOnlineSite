<%@ page language="java" contentType="text/html; charset=EUC-KR"

    pageEncoding="EUC-KR"%>

 

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>LectureOnlineSite</title>
<script type="text/javascript">
</script>
<style type="text/css">
body,p,h1,h2,h3,h4,h5,h6,ul,cl,ol,li,dl,dt,dd,table,th,td,form,fieldset,
legend,input,textarea,button,select{margin:0;padding:0}
img,fieldset{border:0}
cl,ol{list-style:none}

ul{width:1000px;height:30px;background:black;list-style:none;padding-top:20px}
ul li{float:left;margin-left:80px;font-family:dotum}
ul li a{font-size:14px;color:yellow;font-weight:bold;text-decoration:none}
ul rl{float:right;margin-right:30px}
.white a{color:#fff}
em,address{font-style:normal}
a{text-decoration:none}
a:hover,a:active,a:focus{text-decoration:none}
#wrap{float:left;width:100%}
.gnb{float:left;width:100%;height:30px;background:#fbfbfb}
.gnb a{float:left;}
.gnb_center{width:135px;margin:5px auto}
.gnb_right{width:1000px;margin:5px auto}
.gnb_center ul{float:left;}
.gnb_center ul li{float:left;margin-right:10px;}
.gnb_right cl{float:right;}
.gnb_right cl li{float:left;margin-right:10px;}
#container{width:1000px;margin:0 auto}
#header{float:left;width:1000px;_border:1px solid blue}
#header h1{float:left;margin-left:400px;color:#000000;font-size:40px}
.menu{float:left;width:1000px;height:60px;background:#000000}
.left{float:left;width:650px;_border:1px solid blue}
.left_content1{float:left;width:650px;height:200px;margin-top:10px;background:#edeef0}
.right{float:left;width:330px;margin:10px 0 0 20px}
.right_content2,.right_content7{float:left;width:330px;height:140px;margin-bottom:10px;background:#f8f8f8; _border:1px dotted red;_border-radius:700px}
#footer{float:left;width:1000px;height:30px;margin:10px 0 30px 0;background:#e1e1e1}
p{font-size:20px;}
.leftmargin{margin-left:50px}

</style>
</head>
<body>
<div id="wrap">
   
   <div id="container">

      <div id="header">
         <h1>재능 마켓</h1>
      </div>

      <div class="gnb">
         <div class="gnb_right">
            <cl>
               <li>Login</li>
               <li>/</li>
               <li>Register</li>
            </cl>
         </div><!-- gnb_center -->
      </div>

      <div class="menu">
         <ul>           
            <li><a href="#"><p class="leftmargin">Home</p></a></li>
            <li><a href="#"><p>강의</p></a></li>
            <li><a href="#"><p>강사</p></a></li>
            <rl class="white"><a href="#">마이페이지</a></li></rl>
            <rl class="white"><a href="#">장바구니</a></li></rl>                  
         </ul>
      </div>

      <title>회원 정보 수정</title>
    
    <!-- css 파일 분리 -->
    <link href='../../css/join_style.css' rel='stylesheet' style='text/css'/>
 
    <script type="text/javascript">
    
        // 필수 입력정보인 아이디, 비밀번호가 입력되었는지 확인하는 함수
        function checkValue()
        {
            if(!document.userInfo.id.value){
                alert("아이디를 입력하세요.");
                return false;
            }
            
            if(!document.userInfo.password.value){
                alert("비밀번호를 입력하세요.");
                return false;
            }
            
            // 비밀번호와 비밀번호 확인에 입력된 값이 동일한지 확인
            if(document.userInfo.password.value != document.userInfo.passwordcheck.value ){
                alert("비밀번호를 동일하게 입력하세요.");
                return false;
            }
        }
        
        // 취소 버튼 클릭시 로그인 화면으로 이동
        function goLoginForm() {
            location.href="LoginForm.jsp";
        }
    </script>
    
</head>
<body>
    <!-- div 왼쪽, 오른쪽 바깥여백을 auto로 주면 중앙정렬된다.  -->
    <div id="wrap">
        <br><br>
        <b><font size="6" color="gray">정보 수정</font></b>
        <br><br><br>
        
        
        <!-- 입력한 값을 전송하기 위해 form 태그를 사용한다 -->
        <!-- 값(파라미터) 전송은 POST 방식, 전송할 페이지는 JoinPro.jsp -->
        <form method="post" action="../pro/JoinPro.jsp" name="userInfo" 
                onsubmit="return checkValue()">
            <table>
            
                <tr>
                    <td id="title">비밀번호</td>
                    <td>
                        <input type="password" name="password" maxlength="50">
                    </td>
                </tr>
                
                <tr>
                    <td id="title">비밀번호 확인</td>
                    <td>
                        <input type="password" name="passwordcheck" maxlength="50">
                    </td>
                </tr>
                    
                <tr>
                    <td id="title">이름</td>
                    <td>
                        <input type="text" name="name" maxlength="50">
                    </td>
                </tr>
                                              
                <tr>
                    <td id="title">생일</td>
                    <td>
                        <input type="text" name="birthyy" maxlength="4" placeholder="년(4자)" size="6" >
                        <select name="birthmm">
                            <option value="">월</option>
                            <option value="01" >1</option>
                            <option value="02" >2</option>
                            <option value="03" >3</option>
                            <option value="04" >4</option>
                            <option value="05" >5</option>
                            <option value="06" >6</option>
                            <option value="07" >7</option>
                            <option value="08" >8</option>
                            <option value="09" >9</option>
                            <option value="10" >10</option>
                            <option value="11" >11</option>
                            <option value="12" >12</option>
                        </select>
                        <input type="text" name="birthdd" maxlength="2" placeholder="일" size="4" >
                    </td>
                </tr>
                    
                <tr>
                    <td id="title">이메일</td>
                    <td>
                        <input type="text" name="mail1" maxlength="50">@
                        <select name="mail2">
                            <option>naver.com</option>
                            <option>daum.net</option>
                            <option>gmail.com</option>
                            <option>nate.com</option>                        
                        </select>
                    </td>
                </tr>
                    
                <tr>
                    <td id="title">휴대전화</td>
                    <td>
                        <input type="text" name="phone" />
                    </td>
                </tr>
                           </table>
            <br>
            <input type="submit" value="수정"/>  
            <input type="button" value="취소" onclick="goLoginForm()">
        </form>
    </div>

      <div id="footer">
      </div>
   </div>
</div>
</body>
</html>