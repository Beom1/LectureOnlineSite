<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath }</c:set>

<html>
<head>
<meta charset="utf-8" />
<title>LectureOnlineSite</title>
<script type="text/javascript">
	
</script>
<style type="text/css">
body, p, h1, h2, h3, h4, h5, h6, ul, cl, ol, li, dl, dt, dd, table, th,
	td, form, fieldset, legend, input, textarea, button, select {
	margin: 0;
	padding: 0
}

img, fieldset {
	border: 0
}

cl, ol {
	list-style: none
}

ul {
	width: 1000px;
	height: 30px;
	background: black;
	list-style: none;
	padding-top: 20px
}

ul li {
	float: left;
	margin-left: 80px;
	font-family: dotum
}

ul li a {
	font-size: 14px;
	color: yellow;
	font-weight: bold;
	text-decoration: none
}

ul rl {
	float: right;
	margin-right: 30px
}

.white a {
	color: #fff
}

em, address {
	font-style: normal
}

a {
	text-decoration: none
}

a:hover, a:active, a:focus {
	text-decoration: none
}

#wrap {
	float: left;
	width: 100%
}

.gnb {
	float: left;
	width: 100%;
	height: 30px;
	background: #fbfbfb
}

.gnb a {
	float: left;
}

.gnb_center {
	width: 135px;
	margin: 5px auto
}

.gnb_right {
	width: 1000px;
	margin: 5px auto
}

.gnb_center ul {
	float: left;
}

.gnb_center ul li {
	float: left;
	margin-right: 10px;
}

.gnb_right cl {
	float: right;
}

.gnb_right cl li {
	float: left;
	margin-right: 10px;
}

#container {
	width: 1000px;
	margin: 0 auto
}

#header {
	float: left;
	width: 1000px;
	_border: 1px solid blue
}

#header h1 {
	float: left;
	margin-left: 400px;
	color: #000000;
	font-size: 40px
}

.menu {
	float: left;
	width: 1000px;
	height: 60px;
	background: #000000
}

.left {
	float: left;
	width: 650px;
	_border: 1px solid blue
}

.left_content1 {
	float: left;
	width: 650px;
	height: 200px;
	margin-top: 10px;
	background: #edeef0
}

.right {
	float: left;
	width: 330px;
	margin: 10px 0 0 20px
}

.right_content2, .right_content7 {
	float: left;
	width: 330px;
	height: 140px;
	margin-bottom: 10px;
	background: #f8f8f8;
	_border: 1px dotted red;
	_border-radius: 700px
}

#footer {
	float: left;
	width: 1000px;
	height: 30px;
	margin: 10px 0 30px 0;
	background: #e1e1e1
}

p {
	font-size: 20px;
}

.leftmargin {
	margin-left: 50px
}
  body,p,h1,h2,h3,h4,h5,h6,ul,cl,ol,li,dl,dt,dd,table,th,td,form,fieldset,
legend,input,textarea,button,select{margin:0;padding:0}
img,fieldset{border:0}
cl,ol{list-style:none}
cl{float:left;_border:1px;list-style:none}
cl li{float:left;width:250px;height:200px;}
.instructor_name{font-size:20px;font-weight:bold; width:120px; height:30px; background:green; margin-left:30px}
ul{width:1000px;height:30px;background:black;list-style:none;padding-top:20px}
ul li{float:left;margin-left:80px;font-family:dotum}
ul li a{font-size:14px;color:yellow;font-weight:bold;text-decoration:none}
ul rl{float:right;margin-right:30px}
.white a{color:#fff}
em,address{font-style:normal}
a{text-decoration:none}
a:hover,a:active,a:focus{text-decoration:none}
#wrap{float:left;width:100%}
.gnb{float:left;width:100%;height:30px;background:#fbfbfb}
.gnb a{float:left;}
.gnb_center{width:135px;margin:5px auto}
.gnb_right{width:1000px;margin:5px auto}
.gnb_center ul{float:left;}
.gnb_center ul li{float:left;margin-right:10px;}
.gnb_right ol{float:right;}
.gnb_right ol li{float:left;margin-right:10px;}
#container{width:1000px;margin:0 auto}
#header{float:left;width:1000px;_border:1px solid blue}
#header h1{float:left;margin-left:400px;color:#000000;font-size:40px}
.menu{float:left;width:1000px;height:60px;background:#000000}
.left{float:left;width:980px;height:100%;_border:1px solid blue}
.left_content1{float:left;width:980px;height:100%;margin-top:10px;background:#edeef0}

.right{float:left;width:330px;margin:10px 0 0 20px}
.right_content2,.right_content7{float:left;width:330px;height:140px;margin-bottom:10px;background:#f8f8f8; _border:1px dotted red;_border-radius:700px}
#footer{float:left;width:1000px;height:30px;margin:10px 0 30px 0;background:#e1e1e1}
p{font-size:20px;}
  span{font-size:20px;}
.leftmargin{margin-left:50px}

</style>
</head>
<body>
	<div id="wrap">

				<%@ include file="/views/title.jspf" %>


			<div id="wrap">
				<br> <br> <b><font size="6" color="gray">회원가입</font></b> <br>
				<br> <br>


				<form action="${ctx }/join.do" method="post">
					<table class="table">
						<tr>
							<td id="title">아이디</td>
							<td><input type="text" name="id" maxlength="50"> <input
								type="button" value="중복확인"></td>
						</tr>

						<tr>
							<td id="title">비밀번호</td>
							<td><input type="password" name="password" maxlength="50">
							</td>
						</tr>

						<tr>
							<td id="title">이름</td>
							<td><input type="text" name="name" maxlength="50"></td>
						</tr>

						<tr>
							<td id="title">생일</td>
							<td>
						   		<input type="text" name="birth" maxlength="*" placeholer="생년 월일 입력">
							</td>
						</tr>

						<tr>
							<td id="title">이메일</td>
							<td><input type="text" name="mail" maxlength="50">@
								<select name="mail2">
									<option>naver.com</option>
									<option>daum.net</option>
									<option>gmail.com</option>
									<option>nate.com</option>
							</select></td>
						</tr>

						<tr>
							<td id="title">휴대전화</td>
							<td><input type="text" name="phone" /></td>
						</tr>
					</table>
					<br> <input type="submit" value="가입" /> <input type="button"
						value="취소">
				</form>
			</div>

			<div id="footer"></div>
		</div>
	</div>
</body>
</html>
