<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath }</c:set>
<html>
<head>
<meta charset="utf-8" />
<title>LectureOnlineSite</title>
<script type="text/javascript">
</script>
<link href="${ctx}/resources/css/bootstrap.min.css" rel="stylesheet">
<link href="${ctx}/resources/css/style.css" rel="stylesheet">
<style type="text/css">
  
  body,p,h1,h2,h3,h4,h5,h6,ul,cl,ol,li,dl,dt,dd,table,th,td,form,fieldset,
legend,input,textarea,button,select{margin:0;padding:0}
img,fieldset{border:0}
cl,ol{list-style:none}
cl{float:left;_border:1px;list-style:none}
cl li{float:left;width:250px;height:200px;}
.instructor_name{font-size:20px;font-weight:bold; width:120px; height:30px; background:green; margin-left:30px}
ul{width:1000px;height:30px;background:black;list-style:none;padding-top:20px}
ul li{float:left;margin-left:80px;font-family:dotum}
ul li a{font-size:14px;color:yellow;font-weight:bold;text-decoration:none}
ul rl{float:right;margin-right:30px}
.white a{color:#fff}
em,address{font-style:normal}
a{text-decoration:none}
a:hover,a:active,a:focus{text-decoration:none}
#wrap{float:left;width:100%}
.gnb{float:left;width:100%;height:30px;background:#fbfbfb}
.gnb a{float:left;}
.gnb_center{width:135px;margin:5px auto}
.gnb_right{width:1000px;margin:5px auto}
.gnb_center ul{float:left;}
.gnb_center ul li{float:left;margin-right:10px;}
.gnb_right ol{float:right;}
.gnb_right ol li{float:left;margin-right:10px;}
#container{width:1000px;margin:0 auto}
#header{float:left;width:1000px;_border:1px solid blue}
#header h1{float:left;margin-left:400px;color:#000000;font-size:40px}
.menu{float:left;width:1000px;height:60px;background:#000000}
.left{float:left;width:980px;height:100%;_border:1px solid blue}
.left_content1{float:left;width:980px;height:100%;margin-top:10px;background:#edeef0}

.right{float:left;width:330px;margin:10px 0 0 20px}
.right_content2,.right_content7{float:left;width:330px;height:140px;margin-bottom:10px;background:#f8f8f8; _border:1px dotted red;_border-radius:700px}
#footer{float:left;width:1000px;height:30px;margin:10px 0 30px 0;background:#e1e1e1}
p{font-size:20px;}
  span{font-size:20px;}
.leftmargin{margin-left:50px}

</style>
</head>
<body>
<div id="wrap">
	
		<%@ include file="/views/title.jspf" %>


    <div id="lecture"style="border: 1px solid black; width:800px; height:500px; overflow: auto; margin-left:100px;" align="center">   
  <form action="${ctx }/lectureRegister.do?userId=${userId}" method="get">
  <table style="width:80%; height:70%; margin-top:50px; font-size:25px">

    <tr>
      <th>강의명 : </th>
      <td><input type="text" style="width:200px" name="name"></td>
    </tr>
    <tr>
      <th>강사 닉네임 : </th>
      <td><input type="text" style="width:200px" name="instructorNickName"></td>
    </tr>
    <tr>
      <th>강의URL : </th>
      <td><input type="text" style="width:200px" name="url"></td>
    </tr>
    <tr>
      <th>강의시간 : </th>
      <td><input type="text" style="width:200px" name="runtime"></td>
    </tr>

    <tr>
      <th>가격 : </th>
      <td><input type="text" style="width:200px" name="price"></td>
    </tr>
    
    <tr>
      <th>챕터 : </th>
      <td><input type="text" style="width:200px" name="hi"></td>
    </tr>
    
  </table>
  		<input type="submit" value="등록"></input> 
  </form>
       

  </div>
  </div>
</body>
</html>
