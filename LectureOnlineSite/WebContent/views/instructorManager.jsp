<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<c:set var="ctx">${pageContext.request.contextPath }</c:set>
<html>
<head>
<meta charset="utf-8" />
<title>LectureOnlineSite</title>
<script type="text/javascript">
	
</script>
<style type="text/css">
body, p, h1, h2, h3, h4, h5, h6, ul, cl, ol, li, dl, dt, dd, table, th,
	td, form, fieldset, legend, input, textarea, button, select {
	margin: 0;
	padding: 0
}

img, fieldset {
	border: 0
}

cl, ol {
	list-style: none
}

cl {
	float: left;
	_border: 1px;
	list-style: none
}

cl li {
	float: left;
	width: 250px;
	height: 200px;
}

.instructor_name {
	font-size: 20px;
	font-weight: bold;
	width: 120px;
	height: 30px;
	background: green;
	margin-left: 30px
}

ul {
	width: 1000px;
	height: 30px;
	background: black;
	list-style: none;
	padding-top: 20px
}

ul li {
	float: left;
	margin-left: 80px;
	font-family: dotum
}

ul li a {
	font-size: 14px;
	color: yellow;
	font-weight: bold;
	text-decoration: none
}

ul rl {
	float: right;
	margin-right: 30px
}

.white a {
	color: #fff
}

em, address {
	font-style: normal
}

a {
	text-decoration: none
}

a:hover, a:active, a:focus {
	text-decoration: none
}

#wrap {
	float: left;
	width: 100%
}

.gnb {
	float: left;
	width: 100%;
	height: 30px;
	background: #fbfbfb
}

.gnb a {
	float: left;
}

.gnb_center {
	width: 135px;
	margin: 5px auto
}

.gnb_right {
	width: 1000px;
	margin: 5px auto
}

.gnb_center ul {
	float: left;
}

.gnb_center ul li {
	float: left;
	margin-right: 10px;
}

.gnb_right ol {
	float: right;
}

.gnb_right ol li {
	float: left;
	margin-right: 10px;
}

#container {
	width: 1000px;
	margin: 0 auto
}

#header {
	float: left;
	width: 1000px;
	_border: 1px solid blue
}

#header h1 {
	float: left;
	margin-left: 400px;
	color: #000000;
	font-size: 40px
}

.menu {
	float: left;
	width: 1000px;
	height: 60px;
	background: #000000
}

.left {
	float: left;
	width: 980px;
	height: 100%;
	_border: 1px solid blue
}

.left_content1 {
	float: left;
	width: 980px;
	height: 100%;
	margin-top: 10px;
	background: #edeef0
}

.right {
	float: left;
	width: 330px;
	margin: 10px 0 0 20px
}

.right_content2, .right_content7 {
	float: left;
	width: 330px;
	height: 140px;
	margin-bottom: 10px;
	background: #f8f8f8;
	_border: 1px dotted red;
	_border-radius: 700px
}

#footer {
	float: left;
	width: 1000px;
	height: 30px;
	margin: 10px 0 30px 0;
	background: #e1e1e1
}

p {
	font-size: 20px;
}

.leftmargin {
	margin-left: 50px
}

 body,p,h1,h2,h3,h4,h5,h6,ul,cl,ol,li,dl,dt,dd,table,th,td,form,fieldset,
legend,input,textarea,button,select{margin:0;padding:0}
img,fieldset{border:0}
cl,ol{list-style:none}
cl{float:left;_border:1px;list-style:none}
cl li{float:left;width:250px;height:200px;}
.instructor_name{font-size:20px;font-weight:bold; width:120px; height:30px; background:green; margin-left:30px}
ul{width:1000px;height:30px;background:black;list-style:none;padding-top:20px}
ul li{float:left;margin-left:80px;font-family:dotum}
ul li a{font-size:14px;color:yellow;font-weight:bold;text-decoration:none}
ul rl{float:right;margin-right:30px}
.white a{color:#fff}
em,address{font-style:normal}
a{text-decoration:none}
a:hover,a:active,a:focus{text-decoration:none}
#wrap{float:left;width:100%}
.gnb{float:left;width:100%;height:30px;background:#fbfbfb}
.gnb a{float:left;}
.gnb_center{width:135px;margin:5px auto}
.gnb_right{width:1000px;margin:5px auto}
.gnb_center ul{float:left;}
.gnb_center ul li{float:left;margin-right:10px;}
.gnb_right ol{float:right;}
.gnb_right ol li{float:left;margin-right:10px;}
#container{width:1000px;margin:0 auto}
#header{float:left;width:1000px;_border:1px solid blue}
#header h1{float:left;margin-left:400px;color:#000000;font-size:40px}
.menu{float:left;width:1000px;height:60px;background:#000000}
.left{float:left;width:980px;height:100%;_border:1px solid blue}
.left_content1{float:left;width:980px;height:100%;margin-top:10px;background:#edeef0}

.right{float:left;width:330px;margin:10px 0 0 20px}
.right_content2,.right_content7{float:left;width:330px;height:140px;margin-bottom:10px;background:#f8f8f8; _border:1px dotted red;_border-radius:700px}
#footer{float:left;width:1000px;height:30px;margin:10px 0 30px 0;background:#e1e1e1}
p{font-size:20px;}
  span{font-size:20px;}
.leftmargin{margin-left:50px}

</style>
</head>
<body>
	<div id="wrap">

					<%@ include file="/views/title.jspf" %>

			<div>

				<table width="30%" height="10%" style="margin-left: 50px">
					<tr align="left">
						<form action="${ctx }/instructorSearch.do" method="post">
						<td align="left"><input type="text" name="searchInput" placeholder="강사닉네임을 입력하세요.">
						</input></td>
						
						<td align="left"><input class="btn btn-xs btn-default" type="submit" value="검색">
						</input>
						<input type="hidden" name="flag" value="${true }"></input>
						</td>
					<input type="hidden" name="userId" value="${userId }">
					</form>
				</table>
				<table width="500px" height="30%" style="margin-left: 50px">
					</tr>

					<tr align="center">
						<th align="left">강사 닉네임</th>
						
						</td>
						<th align="left">강사 이름
						</td>
						<th align="left">강사 약력
						</td>
						<th align="left">
						</td>
						<th align="left">
						</td>
					</tr>
					<tbody>
					<c:forEach items="${instructors }" var="instructor">
						<tr>
						<td align="left">${instructor.instructorNickName }</td>
						<td align="left">${instructor.instructorName }</td>
						<td align="left">${instructor.instructorProfile }</td>
						
						<td align="left"><a href="${ctx }/instructorModify.do?name=${instructor.instructorNickName}&userId=${userId}">
						<input type="submit" value="수정" />
						</a></td>	
								
						<td align="left"><a href="${ctx }/instructorRemove.do?instructorNickName=${instructor.instructorNickName}&userId=${userId}">
						<input type="submit" value="삭제" />
						</a></td>
						
						</tr>
					</c:forEach>
					</tbody>

				<td align="Right"><a href="${ctx }/ins.do?userId=${userId}">
						<input type="submit" value="등록" />
				</a></td>

				</table>
			</div>
		</div>
		</div>
</body>
</html>
