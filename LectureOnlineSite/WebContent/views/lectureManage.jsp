<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>LectureOnlineSite</title>
<script type="text/javascript">
	
</script>
<style type="text/css">
body, p, h1, h2, h3, h4, h5, h6, ul, cl, ol, li, dl, dt, dd, table, th,
	td, form, fieldset, legend, input, textarea, button, select {
	margin: 0;
	padding: 0
}

img, fieldset {
	border: 0
}

cl, ol {
	list-style: none
}

cl {
	float: left;
	_border: 1px;
	list-style: none
}

cl li {
	float: left;
	width: 250px;
	height: 200px;
}
.instructor_name {
	font-size: 20px;
	font-weight: bold;
	width: 120px;
	height: 30px;
	background: green;
	margin-left: 30px
}

ul {
	width: 1000px;
	height: 30px;
	background: black;
	list-style: none;
	padding-top: 20px
}

ul li {
	float: left;
	margin-left: 80px;
	font-family: dotum
}

ul li a {
	font-size: 14px;
	color: yellow;
	font-weight: bold;
	text-decoration: none
}

ul rl {
	float: right;
	margin-right: 30px
}

.white a {
	color: #fff
}

em, address {
	font-style: normal
}

a {
	text-decoration: none
}

a:hover, a:active, a:focus {
	text-decoration: none
}

#wrap {
	float: left;
	width: 100%
}

.gnb {
	float: left;
	width: 100%;
	height: 30px;
	background: #fbfbfb
}

.gnb a {
	float: left;
}

.gnb_center {
	width: 135px;
	margin: 5px auto
}

.gnb_right {
	width: 1000px;
	margin: 5px auto
}

.gnb_center ul {
	float: left;
}

.gnb_center ul li {
	float: left;
	margin-right: 10px;
}

.gnb_right ol {
	float: right;
}

.gnb_right ol li {
	float: left;
	margin-right: 10px;
}

#container {
	width: 1000px;
	margin: 0 auto
}

#header {
	float: left;
	width: 1000px;
	_border: 1px solid blue
}

#header h1 {
	float: left;
	margin-left: 400px;
	color: #000000;
	font-size: 40px
}

.menu {
	float: left;
	width: 1000px;
	height: 60px;
	background: #000000
}

.left {
	float: left;
	width: 980px;
	height: 100%;
	_border: 1px solid blue
}

.left_content1 {
	float: left;
	width: 980px;
	height: 100%;
	margin-top: 10px;
	background: #edeef0
}

.right {
	float: left;
	width: 330px;
	margin: 10px 0 0 20px
}

.right_content2, .right_content7 {
	float: left;
	width: 330px;
	height: 140px;
	margin-bottom: 10px;
	background: #f8f8f8;
	_border: 1px dotted red;
	_border-radius: 700px
}

#footer {
	float: left;
	width: 1000px;
	height: 30px;
	margin: 10px 0 30px 0;
	background: #e1e1e1
}

p {
	font-size: 20px;
}

.leftmargin {
	margin-left: 50px
}
</style>
</head>
<body>
	<div id="wrap">

			<%@ include file="/views/title.jspf" %>
			<div>

				<table width="30%" height="10%" style="margin-left: 50px">
						<form action="${ctx }/lectureResult.do" method="get">
						<tr align="left">
					<tr align="left">
						<td><select name="category">
								<option value="lectureName">강의명</option>
								<option value="instructorName">강사명</option>
								<option value="lastest" selected>최신순</option>

						</select></td>
						<td align="left"><input type="text" name="search">
						</td>
						<td align="left"><input type="submit" name="submit"
							value="검색"></td>
							<input type="hidden" name="userId" value="${userId }">
						</form>	
				</table>
				<table width="500px" height="30%" style="margin-left: 50px">
					</tr>

					<tr align="center">
						<th align="left">강의번호</th>
						<th align="left" colspan="2">강의명
						</td>
						<th align="left">강사닉네임
						</td>
						<th align="left">챕터
						</td>
						<th align="left">금액
						</td>
						<th align="left">별점
						</td>
					</tr>
					<tbody>
					<c:forEach items="${lectures }" var="lecture">
						<tr align="center">
							<td align="left"><a
								href="${ctx }/lectureInfo.do?lectureNum=${lecture.lectureNum}&userId=${userId}">${lecture.lectureNum }</a></td>
							<td align="left" colspan="2">${lecture.lectureName }</td>
							<td align="left">${lecture.instructorNickName }</td>
							<td align="left">${lecture.lectureChapter }단계</td>
							<td align="left">${lecture.lecturePrice }</td>
							<td align="left">5점</td>
							<td align="left"><a
								href="${ctx }/lectureModify.do?lectureNum=${lecture.lectureNum }&userId=${userId}"><input type="submit" value="수정"></a></td>
							<td align="left"><a
								href="${ctx }/lectureRemove.do?lectureNum=${lecture.lectureNum }&userId=${userId}"><input type="submit" value="삭제"></a></td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
				<a href="${ctx }/lec.do?userId=${userId}">
						<input type="submit" value="등록" />
				</a>
			</div>
		</div>
</body>
</html>
