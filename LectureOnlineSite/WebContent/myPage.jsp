<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>



<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>LectureOnlineSite</title>
<script type="text/javascript">
	
</script>

<style type="text/css">
body, p, h1, h2, h3, h4, h5, h6, ul, cl, ol, li, dl, dt, dd, table, th,
	td, form, fieldset, legend, input, textarea, button, select {
	margin: 0;
	padding: 0
}

img, fieldset {
	border: 0
}

cl, ol {
	list-style: none
}

ul {
	width: 1000px;
	height: 30px;
	background: black;
	list-style: none;
	padding-top: 20px
}

ul li {
	float: left;
	margin-left: 80px;
	font-family: dotum
}

ul li a {
	font-size: 14px;
	color: yellow;
	font-weight: bold;
	text-decoration: none
}

ul rl {
	float: right;
	margin-right: 30px
}

.white a {
	color: #fff
}

em, address {
	font-style: normal
}

a {
	text-decoration: none
}

a:hover, a:active, a:focus {
	text-decoration: none
}

#wrap {
	float: left;
	width: 100%
}

.gnb {
	float: left;
	width: 100%;
	height: 30px;
	background: #fbfbfb
}

.gnb a {
	float: left;
}

.gnb_center {
	width: 135px;
	margin: 5px auto
}

.gnb_right {
	width: 1000px;
	margin: 5px auto
}

.gnb_center ul {
	float: left;
}

.gnb_center ul li {
	float: left;
	margin-right: 10px;
}

.gnb_right cl {
	float: right;
}

.gnb_right cl li {
	float: left;
	margin-right: 10px;
}

#container {
	width: 1000px;
	margin: 0 auto
}

#header {
	float: left;
	width: 1000px;
	_border: 1px solid blue
}

#header h1 {
	float: left;
	margin-left: 400px;
	color: #000000;
	font-size: 40px
}

.menu {
	float: left;
	width: 1000px;
	height: 60px;
	background: #000000
}

.left {
	float: left;
	width: 650px;
	_border: 1px solid blue
}

.left_content1 {
	float: left;
	width: 650px;
	height: 200px;
	margin-top: 10px;
	background: #edeef0
}

#footer {
	float: left;
	width: 1000px;
	height: 30px;
	margin: 10px 0 30px 0;
	background: #e1e1e1
}

p {
	font-size: 20px;
}

.leftmargin {
	margin-left: 50px
}

.title {
	font-size: 40px;
	text-align: center;
}

.id {
	font-size: 30px;
}

.name {
	font-size: 30px;
}

.email {
	font-size: 30px;
}

.gender {
	font-size: 30px;
}

.birth {
	font-size: 30px;
}

.phone {
	font-size: 30px;
}

.lectureName {
	font-size: 30px;
}

.title {
	font-size: 40px;
	text-align: center;
}

.id {
	font-size: 30px;
}

.name {
	font-size: 30px;
}

.email {
	font-size: 30px;
}

.gender {
	font-size: 30px;
}

.birth {
	font-size: 30px;
}

.phone {
	font-size: 30px;
}

.lectureName {
	font-size: 30px;
}

.evaluate {
	font-size: 15px;
}

.modify {
	font-size: 15px;
}

.leave {
	font-size: 15px;
}

#layout1 {
	width: 450px;
	height: 450px;
	padding: 20px;
	margin-bottom: 20px;
	border: 1px solid black;
}

#layout2 {
	width: 450px;
	padding: 20px;
	margin-top: 20px;
	margin-bottom: 20px;
	border: 1px solid black;
}
</style>

</head>
<body>
	<div id="wrap">

		<div id="container">

			<div id="header">
				<h1>재능 마켓</h1>
			</div>

			<div class="gnb">
				<div class="gnb_right">
					<cl>
					<li>Login</li>
					<li>/</li>
					<li>Register</li>
					</cl>
				</div>
				<!-- gnb_center -->
			</div>

			<div class="menu">
				<ul>
					<li><a href="#"><p class="leftmargin">Home</p></a></li>
					<li><a href="#"><p>강의</p></a></li>
					<li><a href="#"><p>강사</p></a></li>
					<rl class="white"> <a href="#">마이페이지</a>
					</li>
					</rl>
					<rl class="white"> <a href="#">장바구니</a>
					</li>
					</rl>
				</ul>
			</div>
			<title>HTML, CSS and JavaScript demo</title>
			</head>

			<body>
				<!-- Start your code here -->

				<div id=area>
	
				<div id="layout1">
					<p class="title">내 정보</p>
					<p class="id">아이디</p>
					<p class="name">이름</p>
					<p class="email">이메일</p>
					<p class="gender">성별</p>
					<p class="birth">생년월일</p>
					<p class="phone">전화번호</p>
					<button type="button" class="modify">정보 수정</button>
					<button type="button" class="leave">회원 탈퇴</button>
				</div>

				<div id="layout2">
					<p class="title">내 강의</p>
					<p class="lectureName">뜨개질</p>
					<button type="button" class="evaluate">강의 평가</button>
					<p class="lectureName">기타</p>
					<button type="button" class="evaluate">강의 평가</button>
					<p class="lectureName">컴활1급 자격증</p>
					<button type="button" class="evaluate">강의 평가</button>
				</div>
				</div>

				<!-- End your code here -->
			</body>
</html>